import React from "react";
import { TestersLanding } from "@/components/testersLanding";

const TestersPage: React.FC = () => {
  return <TestersLanding />;
};

export default TestersPage;
