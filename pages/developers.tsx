import React from "react";
import { DevelopersLanding } from "@/components/developersLanding";

const ProgrammersPage: React.FC = () => {
  return <DevelopersLanding />;
};

export default ProgrammersPage;
