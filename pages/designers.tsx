import React from "react";
import { DesignersLanding } from "@/components/designersLanding";

const DesignersPage: React.FC = () => {
  return <DesignersLanding />;
};

export default DesignersPage;
