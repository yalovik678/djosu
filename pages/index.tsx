"use client";
import Header from "@/components/commonLanding/header";
import Benefits from "@/components/commonLanding/benefits";
import BeforeVacancies from "@/components/commonLanding/beforeVacancies";
import Vacancies from "@/components/commonLanding/vacancies";
import BeforeFeedback from "@/components/commonLanding/beforeFeedback";
import AboutUs from "@/components/commonLanding/aboutUs";
import Feedback from "@/components/commonLanding/feedback";
import { RefObject, useEffect, useRef, useState } from "react";
import CheckoutQA from "@/components/commonLanding/checkoutQA";
import BeforeQADesktop from "@/components/commonLanding/beforeQADesktop";

export default function Home() {
  const headerRef = useRef<HTMLDivElement>(null);
  const feedbackRef = useRef<HTMLDivElement>(null);
  const aboutUsRef = useRef<HTMLDivElement>(null);
  const vacanciesRef = useRef<HTMLDivElement>(null);
  const benefitsRef = useRef<HTMLDivElement>(null);
  const [selectedRef, setSelectedRef] =
    useState<RefObject<HTMLDivElement>>(headerRef);

  useEffect(() => {
    if (selectedRef) {
      selectedRef.current?.scrollIntoView({ behavior: "smooth" });
    }
  }, [selectedRef]);

  const handleSetRef = (id: number) => {
    switch (id) {
      case 0:
        setSelectedRef(feedbackRef);
        break;
      case 1:
        setSelectedRef(aboutUsRef);
        break;
      case 2:
        setSelectedRef(vacanciesRef);
        break;
      case 3:
        setSelectedRef(benefitsRef);
        break;
      default:
        console.log("here");
        break;
    }
  };

  return (
    <div>
      {/*<div ref={headerRef}>*/}
      <Header setSelectedRef={handleSetRef} />
      {/*</div>*/}
      <div ref={benefitsRef}>
        <Benefits />
      </div>
      <div>
        <BeforeVacancies />
      </div>
      <div ref={vacanciesRef}>
        <Vacancies />
      </div>
      <BeforeFeedback />
      <div ref={feedbackRef}>
        <Feedback />
      </div>
      <BeforeQADesktop />
      <CheckoutQA />
      <div ref={aboutUsRef}>
        <AboutUs commonLanding={true} />
      </div>
    </div>
  );
}
