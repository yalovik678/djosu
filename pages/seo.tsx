import React from "react";
import { SEOLanding } from "@/components/seoLanding";

const SEOPage: React.FC = () => {
  return <SEOLanding />;
};

export default SEOPage;
