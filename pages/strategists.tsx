import React from "react";
import { StrategistsLanding } from "@/components/strategistslanding";

const StrategistsPage: React.FC = () => {
  return <StrategistsLanding />;
};

export default StrategistsPage;
