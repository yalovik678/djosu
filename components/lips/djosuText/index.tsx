import React, { FC } from "react";
import styles from "./styles.module.scss";
import cn from "classnames";

interface DjosuTextProps {
  className?: string;
}

const DjosuText: FC<DjosuTextProps> = ({ className }) => {
  return (
    <div className={cn(styles.container, className)}>
      <span>DJOSU</span>
      <span>DJOSU</span>
      <span>DJOSU</span>
    </div>
  );
};

export default DjosuText;
