"use client";
import { FC, useState } from "react";

import menu from "@/public/assets/designers-icons/header/menu.svg";
import logo from "@/public/assets/designers-icons/header/logo.png";
import glass from "@/public/assets/icons/glass-21.png";
import cubeRight from "@/public/assets/developers-icons/header/cube-right.png";
import cubeLeft from "@/public/assets/developers-icons/header/cube-left.png";

import styles from "./styles.module.scss";
import Image from "next/image";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import cn from "classnames";
import BurgerMenu from "@/components/lips/menu/components/burgerMenu";

interface IMenu {
  setSelectedRef: (id: number) => void;
  className: string;
  classNameBurgerMenu?: string;
}

const Menu: FC<IMenu> = ({
  setSelectedRef,
  className,
  classNameBurgerMenu,
}) => {
  const { isMobile } = useResizeWindow();
  const [isOpenBurgerMenu, setIsOpenBurgerMenu] = useState(false);

  const handleFeedbackClick = () => {
    setSelectedRef(0);
  };

  const handleAboutUsClick = () => {
    setSelectedRef(1);
  };

  const handleVacanciesClick = () => {
    setSelectedRef(2);
  };

  const handleBenefitsClick = () => {
    setSelectedRef(3);
  };

  const handleClickBurgerMenu = () => {
    setIsOpenBurgerMenu((prevState) => !prevState);
  };

  const handleClickMenuItems = (id: number) => {
    switch (id) {
      case 0:
        setSelectedRef(0);
        break;
      case 1:
        setSelectedRef(1);
        break;
      case 2:
        setSelectedRef(2);
        break;
      case 3:
        setSelectedRef(3);
        break;
    }
  };

  return (
    <div className={cn(styles.menu, className)}>
      {isMobile ? (
        <>
          <button
            onClick={() => {
              handleClickBurgerMenu();
            }}
          >
            <Image src={menu} alt="menu" className={styles.menuBurger} />
          </button>
        </>
      ) : (
        <>
          <button onClick={handleFeedbackClick}>Vacancies</button>
          <button onClick={handleAboutUsClick}>Benefits</button>
          <button onClick={handleVacanciesClick}>Feedback</button>
          <button onClick={handleBenefitsClick}>Q&A</button>
        </>
      )}
      <Image src={logo} alt="logo" className={styles.logo} />
      {isOpenBurgerMenu && (
        <BurgerMenu
          onClick={handleClickMenuItems}
          className={classNameBurgerMenu}
        />
      )}
    </div>
  );
};
export default Menu;
