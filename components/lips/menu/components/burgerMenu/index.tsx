import React, { FC } from "react";
import styles from "./styles.module.scss";
import cn from "classnames";

interface BurgerMenuProps {
  onClick: (id: number) => void;
  className?: string;
}

const itemsMenu = ["Vacancies", "Benefits", "Feedback", "Q&A"];

const BurgerMenu: FC<BurgerMenuProps> = ({ onClick, className }) => {
  return (
    <div className={cn(styles.containerMenu, className)}>
      {itemsMenu.map((name, index) => (
        <button
          key={index}
          className={styles.btn}
          onClick={() => onClick(index)}
        >
          {name}
        </button>
      ))}
    </div>
  );
};

export default BurgerMenu;
