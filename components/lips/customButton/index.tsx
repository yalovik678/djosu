import React, { FC } from "react";
import styles from "./styles.module.scss";
import cn from "classnames";

interface CustomButtonProps {
  text: string;
  className?: string;
  onClick?: () => void;
}

const CustomButton: FC<CustomButtonProps> = ({ text, className, onClick }) => {
  return (
    <button className={cn(styles.btn, className)} onClick={onClick}>
      {text}
    </button>
  );
};

export default CustomButton;
