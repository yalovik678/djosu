import { FC } from "react";
import branch from "@/public/assets/developers-icons/benefits/branch.png";
import branchMobile from "@/public/assets/developers-icons/benefits/branch-mobile.png";
import blurRightBottom from "@/public/assets/developers-icons/benefits/blur-right-bottom.svg";
import blurRightTop from "@/public/assets/developers-icons/benefits/blur-right-top.svg";
import blurleft from "@/public/assets/developers-icons/benefits/blur-left.svg";
import djosu from "@/public/assets/developers-icons/benefits/djosu.png";
import glass from "@/public/assets/icons/glass-13.png";
import Image from "next/image";

import styles from "./styles.module.scss";
import cn from "classnames";
import CustomButton from "../../../lips/customButton";

const Benefits: FC = () => {
  return (
    <div className={styles.containerBenefits}>
      <div className={styles.text}>
        <div className={styles.tx1}>In our long time in the market, </div>
        <div className={styles.tx2}>
          we have been able to help many people.{" "}
        </div>
        <div className={styles.tx3}>We can help you too! </div>
      </div>
      <div className={cn(styles.containerInfo, styles.cnt1)}>
        <div className={styles.number}>1</div>
        <div className={styles.textFlexible}>Flexible working schedule</div>
      </div>
      <div className={cn(styles.containerInfo, styles.cnt2)}>
        <div className={styles.number}>2</div>
        <div className={styles.textSalary}>High salary</div>
        <div className={styles.textPlus}>+</div>
        <div className={styles.textPleasant}>Pleasant bonuses</div>
      </div>
      <div className={cn(styles.containerInfo, styles.cnt3)}>
        <div className={styles.number}>3</div>
        <div className={styles.textDevelopment}>Development opportunity</div>
      </div>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <Image src={branch} alt="branch" className={styles.branch} />
      <Image
        src={branchMobile}
        alt="branchMobile"
        className={styles.branchMobile}
      />
      <Image src={glass} alt="glass" className={styles.glass} />
      <Image
        src={blurRightBottom}
        alt="blurRight"
        className={styles.blurRight}
      />{" "}
      <Image
        src={blurRightTop}
        alt="blurRightTop"
        className={styles.blurRightTop}
      />
      <Image src={blurleft} alt="blurleft" className={styles.blurleft} />
      <Image src={djosu} alt="djosu" className={styles.djosu} />
    </div>
  );
};

export default Benefits;
