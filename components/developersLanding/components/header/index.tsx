"use client";
import { FC } from "react";

import menu from "@/public/assets/designers-icons/header/menu.svg";
import logo from "@/public/assets/designers-icons/header/logo.png";
import glass from "@/public/assets/icons/glass-21.png";
import cubeRight from "@/public/assets/developers-icons/header/cube-right.png";
import cubeLeft from "@/public/assets/developers-icons/header/cube-left.png";

import styles from "./styles.module.scss";
import Image from "next/image";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import Menu from "@/components/lips/menu";

interface IHeaderProps {
  setSelectedRef: (id: number) => void;
}

const Header: FC<IHeaderProps> = ({ setSelectedRef }) => {
  const { isMobile } = useResizeWindow();

  const handleFeedbackClick = () => {
    setSelectedRef(0);
  };

  const handleAboutUsClick = () => {
    setSelectedRef(1);
  };

  const handleVacanciesClick = () => {
    setSelectedRef(2);
  };

  const handleBenefitsClick = () => {
    setSelectedRef(3);
  };

  return (
    <div className={styles.wrapper}>
      <Menu
        setSelectedRef={setSelectedRef}
        className={styles.menu}
        classNameBurgerMenu={styles.burgerMenu}
      />

      <div className={styles.title}>
        <div className={styles.text1}>programmer is a creator </div>
        <div className={styles.text2}>of a world of digital possibilities</div>
      </div>

      <div className={styles.extraMenu}>
        <button className={styles.btn1} onClick={handleFeedbackClick}>
          Vacancies
        </button>
        <button className={styles.btn2} onClick={handleAboutUsClick}>
          Benefits
        </button>
        <button className={styles.btn3} onClick={handleVacanciesClick}>
          Feedback
        </button>
        <button className={styles.btn4} onClick={handleBenefitsClick}>
          Q&A
        </button>
      </div>

      <Image src={glass} alt="glass" className={styles.glass} />
      <Image src={cubeLeft} alt="cube" className={styles.cubeLeft} />
      <div className={styles.blurLeft} />
      <div className={styles.blurRight} />
      <Image src={cubeRight} alt="cube" className={styles.cubeRight} />
    </div>
  );
};
export default Header;
