import { IQAData } from "@/components/designersLanding/components/qa";
import styles from "./styles.module.scss";

export const qaDataDevelopers: IQAData[] = [
  {
    id: 1,
    question:
      "What is the structure of salary and bonus system at different positions in the company?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          <span>
            Djosu uses a remuneration system that includes a fixed salary and
            bonuses for the fulfillment of certain tasks.
          </span>
        </div>
        <div className={styles.text}>
          The salary depends on the position and qualifications of the employee,
          while bonuses are paid for achieving certain results. In addition, the
          company provides bonuses for overtime and participation in successful
          projects.
        </div>
      </div>
    ),
  },
  {
    id: 2,
    question:
      "What opportunities for professional growth and development does Djosu provide to its employees?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          Djosu offers its employees{" "}
          <span>
            many opportunities for professional growth and development.
          </span>
          We are constantly working to improve our products and services, which
          requires our employees to continuously train and improve their skills.
        </div>
        <div className={styles.text}>
          Djosu has internal <span>training programs</span> to help employees
          expand their knowledge and skills. In addition, we actively involve
          our employees in various projects, which allows them to gain new
          experience and knowledge. We also provide opportunities for career
          growth and development within the company.
        </div>
      </div>
    ),
  },
  {
    id: 3,
    question:
      "What social packages and benefits are provided to Djosu employees?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          Djosu provides its employees with a wide range of social packages and
          benefits, which include:
        </div>
        <ul>
          <li>
            Health insurance covering both the employee and his/her family.
          </li>

          <li>Paid holidays and sick days.</li>

          <li>Opportunity to work remotely or on a flexible schedule.</li>
          <li>
            Discounts on the company&apos;s products and services, as well as on
            partners&apos; goods.
          </li>
          <li>Training and development of professional skills.</li>
          <li>
            Participation in corporate events and team-building activities.
          </li>
        </ul>
      </div>
    ),
  },
  {
    id: 4,
    question:
      "What corporate events and activities are organized to maintain team spirit and motivate employees?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          Djosu actively supports corporate culture and organises various events
          for its employees. This helps to maintain team spirit and motivate
          employees to achieve common goals. Some of these activities include:
        </div>
        <ul>
          <li>
            Regular corporate events such as holidays, sports competitions and
            team building events.
          </li>

          <li>
            Attending professional conferences and exhibitions where employees
            can share their experiences and knowledge.
          </li>

          <li>
            Organising training seminars and workshops for employees to develop
            their professional skills.
          </li>
          <li>
            Organising cultural events such as visits to theatres, museums and
            exhibitions.
          </li>
          <li>
            Supporting employee volunteering and encouraging participation in
            charitable projects.
          </li>
        </ul>
      </div>
    ),
  },
];
