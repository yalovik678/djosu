import React, { FC } from "react";
import rectanglesGroup from "@/public/assets/developers-icons/vacancies/group-rectangles.png";
import text1 from "@/public/assets/developers-icons/vacancies/text1.png";
import text2 from "@/public/assets/developers-icons/vacancies/text2.png";
import text3 from "@/public/assets/developers-icons/vacancies/text3.png";
import star from "@/public/assets/developers-icons/vacancies/star.png";
import spiral from "@/public/assets/icons/spiral.png";
import smile from "@/public/assets/developers-icons/vacancies/smile.png";
import glass from "@/public/assets/icons/glass-13.png";
import Image from "next/image";

import styles from "./styles.module.scss";
import CustomButton from "../../../lips/customButton";

const Vacancies: FC = () => {
  return (
    <div className={styles.containerVacancies}>
      <Image
        src={rectanglesGroup}
        alt="rectanglesGroup"
        className={styles.rectanglesGroup}
      />
      <Image src={text1} alt="text1" className={styles.text1} />
      <Image src={text2} alt="text2" className={styles.text2} />
      <Image src={text3} alt="text3" className={styles.text3} />
      <div className={styles.vacancies}>
        <div className={styles.tx1}>Front-end developer</div>
        <div className={styles.tx2}>Python developer</div>
        <div className={styles.tx1}>Software Engineer</div>
        <div className={styles.tx2}>Full Stack Developer</div>
      </div>
      <Image src={star} alt="star" className={styles.star} />
      <Image src={glass} alt="glass" className={styles.glass} />
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <Image src={smile} alt="smile" className={styles.smile} />
      <Image src={spiral} alt="spiral" className={styles.spiral} />
    </div>
  );
};

export default Vacancies;
