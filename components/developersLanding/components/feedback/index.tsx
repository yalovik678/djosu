import { FC, useRef, useState } from "react";
import subtract from "@/public/assets/developers-icons/feedback/subtract.svg";
import feedbackForm from "@/public/assets/developers-icons/feedback/feedback-form.png";
import arrowLeft from "@/public/assets/developers-icons/feedback/arrow-left.svg";
import arrowRight from "@/public/assets/developers-icons/feedback/arrow-right.svg";
import glass from "@/public/assets/developers-icons/feedback/glass.png";
import substance from "@/public/assets/icons/substance-355.png";
import liquida from "@/public/assets/icons/mujer-liquida-4.png";
import star from "@/public/assets/icons/star-white.svg";
import glassMobile from "@/public/assets/icons/glass-s56.png";

import Image from "next/image";

import styles from "./styles.module.scss";
import SliderFeedbck from "@/components/developersLanding/components/feedback/components/sliderFeedbacks";
import { Feedbacks } from "@/components/developersLanding/components/feedback/constants/Feedbacks";
import CustomButton from "@/components/lips/customButton";

const Feedback: FC = () => {
  const [activeIndex, setActiveIndex] = useState(0);
  const swiperRef = useRef<any>(null);

  const handleSetFeedback = (index: number) => {
    setActiveIndex(index);
  };

  const handleClickArrowLeft = () => {
    const newIndex = activeIndex === 0 ? Feedbacks.length - 1 : activeIndex - 1;
    setActiveIndex(newIndex);
    if (swiperRef.current) {
      swiperRef.current.swiper.slideTo(newIndex, 200, true);
    }
  };

  const handleClickArrowRight = () => {
    const newIndex = activeIndex === Feedbacks.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(newIndex);
    if (swiperRef.current) {
      swiperRef.current.swiper.slideTo(newIndex, 200, true);
    }
  };
  return (
    <div className={styles.containerFeedback}>
      <div className={styles.profile}>
        <Image src={subtract} alt="subtract" className={styles.subtract} />
        <div className={styles.info}>
          <div className={styles.photo}>
            <button className={styles.arrow} onClick={handleClickArrowLeft}>
              <Image src={arrowLeft} alt="arrowLeft" className={styles.arrow} />
            </button>
            <div className={styles.slider}>
              <SliderFeedbck
                feedbacks={Feedbacks}
                selectedFeedback={activeIndex}
                setActiveIndex={handleSetFeedback}
                swiperRef={swiperRef}
              />
            </div>
            <button className={styles.arrow} onClick={handleClickArrowRight}>
              <Image src={arrowRight} alt="arrowRight" />
            </button>
          </div>
          <div className={styles.name}>{Feedbacks[activeIndex].name}</div>
        </div>
      </div>
      <div className={styles.feedback}>
        <Image
          src={feedbackForm}
          alt="feedbackForm"
          className={styles.feedbackForm}
        />
        <div className={styles.containerText}>
          {Feedbacks[activeIndex].feedback.map((text, index) => (
            <div key={index} className={styles.text}>
              {text}
            </div>
          ))}
        </div>
        <div className={styles.line} />
      </div>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />

      <Image src={substance} alt="substance" className={styles.substance} />
      <Image src={liquida} alt="liquida" className={styles.liquida} />
      <Image src={star} alt="star" className={styles.star} />
      <Image src={glass} alt="glass" className={styles.glass} />
      <Image
        src={glassMobile}
        alt="glassMobile"
        className={styles.glassMobile}
      />
      <div className={styles.blur} />
    </div>
  );
};

export default Feedback;
