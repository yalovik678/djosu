import React, { FC, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import styles from "./styles.module.scss";

import Image from "next/image";
import { Swiper as SwiperCore } from "swiper/types";

export interface IFeedbacks {
  id: number;
  photo: string;
  name: string;
  feedback: string[];
}

interface ISlider {
  feedbacks: IFeedbacks[];
  selectedFeedback: number;
  setActiveIndex: (value: number) => void;
  swiperRef: any;
}

const SliderFeedbck: FC<ISlider> = ({
  feedbacks,
  selectedFeedback,
  setActiveIndex,
  swiperRef,
}) => {
  const handleSlideChange = (swiper: SwiperCore) => {
    setActiveIndex(swiper.realIndex);
  };

  return (
    <div className={styles.containerMobile}>
      <Swiper
        initialSlide={selectedFeedback}
        onSlideChange={handleSlideChange}
        slidesPerView={1}
        watchSlidesProgress
        speed={200}
        mousewheel
        keyboard
        loop
        className={styles.mainPhotoSlider}
        ref={swiperRef}
      >
        {feedbacks.map((feedback) => (
          <SwiperSlide key={feedback.id} className={styles.imagesWrapper}>
            <label className={styles.slideContainer}>
              <Image
                src={feedback.photo}
                unoptimized={false}
                alt="photo"
                className={styles.scaleAnimation}
              />
            </label>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default SliderFeedbck;
