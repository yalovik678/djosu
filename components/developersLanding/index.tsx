"use client";

import AboutUs from "@/components/commonLanding/aboutUs";
import { FC, RefObject, useEffect, useRef, useState } from "react";
import Feedback from "@/components/developersLanding/components/feedback";
import BeforeQA from "@/components/developersLanding/components/beforeQA";
import QA from "@/components/developersLanding/components/qa";
import Benefits from "@/components/developersLanding/components/benefits";
import Header from "@/components/developersLanding/components/header";
import Vacancies from "./components/vacancies";

export const DevelopersLanding: FC = () => {
  const headerRef = useRef<HTMLDivElement>(null);
  const feedbackRef = useRef<HTMLDivElement>(null);
  const QARef = useRef<HTMLDivElement>(null);
  const vacanciesRef = useRef<HTMLDivElement>(null);
  const benefitsRef = useRef<HTMLDivElement>(null);
  const [selectedRef, setSelectedRef] =
    useState<RefObject<HTMLDivElement>>(headerRef);

  useEffect(() => {
    if (selectedRef) {
      selectedRef.current?.scrollIntoView({ behavior: "smooth" });
    }
  }, [selectedRef]);

  const handleSetRef = (id: number) => {
    switch (id) {
      case 0:
        setSelectedRef(vacanciesRef);
        break;
      case 1:
        setSelectedRef(benefitsRef);
        break;
      case 2:
        setSelectedRef(feedbackRef);
        break;
      case 3:
        setSelectedRef(QARef);
        break;
      default:
        console.log("here");
        break;
    }
  };

  return (
    <div>
      <div ref={headerRef}>
        <Header setSelectedRef={handleSetRef} />
      </div>
      <div ref={vacanciesRef}>
        <Vacancies />
      </div>

      <div ref={benefitsRef}>
        <Benefits />
      </div>

      <div ref={feedbackRef}>
        <Feedback />
      </div>
      <BeforeQA />
      <div ref={QARef}>
        <QA />
      </div>
      <AboutUs />
    </div>
  );
};
