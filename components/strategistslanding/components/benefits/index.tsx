import React, { FC } from "react";
import face from "@/public/assets/strategists-icons/benefits/face.png";
import glass from "@/public/assets/strategists-icons/benefits/glass.png";
import Image from "next/image";

import styles from "./styles.module.scss";
import cn from "classnames";
import CustomButton from "../../../lips/customButton";

const Benefits: FC = () => {
  return (
    <div className={styles.containerBenefits}>
      <div className={styles.mainBlock}>
        <div className={styles.face}>
          <Image src={face} alt="face" />
        </div>
        <div className={styles.info}>
          <div className={styles.schedule}>
            <div className={styles.number}>1</div>
            <div className={styles.text}>Flexible working schedule</div>
          </div>
          <div className={styles.salary}>
            <div className={styles.number}>2</div>
            <div className={styles.text}>High salary</div>
          </div>
          <div className={styles.bonuses}>
            <div className={styles.number}>3</div>
            <div className={styles.text}>Pleasant bonuses</div>
          </div>
          <div className={styles.opportunity}>
            <div className={styles.number}>4</div>
            <div className={styles.text}>Development opportunity</div>
          </div>
        </div>
      </div>
      <div className={styles.blur} />
      <Image src={glass} alt="glass" className={styles.glass} />
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
    </div>
  );
};

export default Benefits;
