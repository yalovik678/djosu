import React, { FC } from "react";

import rectangle from "@/public/assets/strategists-icons/vacancy/rectangle.png";
import ring from "@/public/assets/strategists-icons/vacancy/ring.png";
import cylinder from "@/public/assets/strategists-icons/vacancy/cylinder.png";
import sphere from "@/public/assets/strategists-icons/vacancy/sphere.png";
import spiral from "@/public/assets/strategists-icons/vacancy/spiral.png";
import spiralMobile from "@/public/assets/strategists-icons/vacancy/spiral-mobile.png";
import Image from "next/image";

import styles from "./styles.module.scss";

const Vacancies: FC = () => {
  return (
    <div className={styles.containerVacancies}>
      <div className={styles.textInOur}>In our long time in the market,</div>
      <div className={styles.textWeHave}>
        we have been able to help many people.
      </div>
      <div className={styles.textWeCan}>We can help you too!</div>
      <div className={styles.blockText1}>
        <div className={styles.tx1}>In our long time in the market,</div>
        <div className={styles.tx2}>we have been able to help many people.</div>
        <div className={styles.tx3}>We can help you too!</div>
      </div>
      <div className={styles.blockText2}>
        <div className={styles.tx1}>
          Developing and managing digital marketing campaigns.
        </div>
        <div className={styles.tx2}>
          Planning and implementation of social media strategy.
        </div>
        <div className={styles.tx3}>
          Researching and analysing competitor activity.
        </div>
      </div>
      <div className={styles.title}>
        <Image src={rectangle} alt="rectangle" className={styles.rectangle} />
        <div className={styles.text}>Social Media Strategist</div>
      </div>
      <div className={styles.cylinder}>
        <Image src={cylinder} alt="cylinder" />
        <div className={styles.text}>respond</div>
      </div>
      <Image src={ring} alt="ring" className={styles.ring} />
      <Image src={sphere} alt="sphere" className={styles.sphere} />
      <Image src={spiral} alt="spiral" className={styles.spiral} />
      <Image
        src={spiralMobile}
        alt="spiralMobile"
        className={styles.spiralMobile}
      />
      <div className={styles.blur}></div>
    </div>
  );
};

export default Vacancies;
