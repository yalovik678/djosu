import React, { FC, useRef, useState } from "react";
import photo1 from "@/public/assets/testers-icons/feedback/photo1.png";
import photo2 from "@/public/assets/testers-icons/feedback/photo2.png";
import photo3 from "@/public/assets/testers-icons/feedback/photo3.png";
import djosu from "@/public/assets/strategists-icons/feedback/djosu.png";
import ringByPhoto from "@/public/assets/strategists-icons/feedback/ring-photo1.png";
import spiralByPhoto from "@/public/assets/strategists-icons/feedback/spiral-photo2.png";
import byPhoto3 from "@/public/assets/strategists-icons/feedback/353-photo3.png";
import arrow from "@/public/assets/strategists-icons/feedback/arrow.png";
import line from "@/public/assets/testers-icons/feedback/line.svg";
import Image from "next/image";

import styles from "./styles.module.scss";
import cn from "classnames";
import CustomButton from "@/components/lips/customButton";

const Feedback: FC = () => {
  return (
    <div className={styles.containerFeedback}>
      <Image src={djosu} alt="djosu" className={styles.djosu} />
      <div className={cn(styles.title1, styles.title)}>
        Djosu is an innovative IT company specialising in the development and
        implementation of cutting-edge technologies.
      </div>
      <div className={cn(styles.title2, styles.title)}>
        We offer a wide range of services including website development, mobile
        app development, systems integration and more.
      </div>
      <div className={styles.feedback1}>
        <div className={styles.photo}>
          <Image
            src={ringByPhoto}
            alt="ringByPhoto"
            className={styles.imgByPhoto}
          />
          <Image src={photo1} alt="photo1" className={styles.photo1} />
        </div>
        <div className={styles.name}>Aedyn Vogal</div>
        <Image src={line} alt="line" className={styles.line} />
        <div className={cn(styles.text, styles.text1)}>
          I work as a social media strategist at Djosu and I want to share my
          experience.
        </div>
        <div className={cn(styles.text, styles.text2)}>
          At Djosu I found a friendly atmosphere and professional colleagues.
        </div>
        <div className={cn(styles.text, styles.text3)}>
          We are always ready to help each other and strive to achieve common
          goals. Thanks to this, the work becomes pleasant and productive.
        </div>
      </div>
      <div className={cn(styles.feedback2, styles.feedback1)}>
        <div className={styles.photo}>
          <Image src={photo2} alt="photo1" className={styles.photo1} />
          <Image
            src={spiralByPhoto}
            alt="spiralByPhoto"
            className={styles.imgByPhoto}
          />
        </div>
        <div className={styles.name}>Jubin Severson</div>
        <Image src={line} alt="line" className={styles.line} />
        <div className={cn(styles.text, styles.text1)}>
          Our team uses various tools and platforms to promote products and
          services.
        </div>
        <div className={cn(styles.text, styles.text2)}>
          We constantly study new trends and adapt our strategies to changing
          market conditions. This allows us to remain competitive and attract
          new customers.
        </div>
      </div>
      <div className={cn(styles.feedback3, styles.feedback1)}>
        <div className={styles.photo}>
          <Image src={photo3} alt="photo1" className={styles.photo1} />
        </div>
        <Image src={byPhoto3} alt="byPhoto3" className={styles.imgByPhoto} />
        <div className={styles.name}>Stefano Vogal</div>
        <Image src={line} alt="line" className={styles.line} />
        <div className={cn(styles.text, styles.text1)}>
          I would like to make a special mention of our work with clients.
        </div>
        <div className={cn(styles.text, styles.text2)}>
          We always take into account their needs and preferences in order to
          create high-quality content and advertising materials.
        </div>
        <div className={cn(styles.text, styles.text3)}>
          This helps us to establish long-term relationships with our clients
          and build their trust.
        </div>
      </div>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <Image src={arrow} alt="arrow" className={styles.arrow} />
      <div className={styles.blur} />
    </div>
  );
};

export default Feedback;
