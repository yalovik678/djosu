import { FC, useState } from "react";
import arrow from "@/public/assets/strategists-icons/qa/arrow.svg";
import glass13 from "@/public/assets/icons/glass-13.png";
import glass351 from "@/public/assets/icons/glass-351.png";
import Image from "next/image";
import cn from "classnames";

import styles from "./styles.module.scss";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import { qaDataStrategists } from "./constants/qaDataStrategists";

export interface IQAData {
  id: number;
  question: string;
  answer: JSX.Element;
}

const QA: FC = () => {
  const { isMobile } = useResizeWindow();
  const [selectedQA, setSelectedQA] = useState(0);

  const handleClick = (id: number) => {
    if (selectedQA === id) {
      setSelectedQA(0);
    } else {
      setSelectedQA(id);
    }
  };

  return (
    <div className={styles.containerQA}>
      <div className={styles.text1}>
        Djosu invites talented IT specialists to join its team!
      </div>
      {!isMobile && (
        <>
          <div className={styles.text2}>
            With us you will find excellent working conditions,
          </div>
          <div className={styles.text3}>
            interesting projects and opportunity for professional growth.
          </div>
        </>
      )}
      <div className={styles.QAs}>
        {qaDataStrategists.map((qa) => (
          <div key={qa.id} className={styles.QA}>
            <div className={styles.question} onClick={() => handleClick(qa.id)}>
              <div className={styles.text}>{qa.question}</div>
              <Image
                src={arrow}
                alt="arrow"
                className={cn(styles.arrow, {
                  [styles.activeArrow]: selectedQA === qa.id,
                })}
              />
            </div>
            {selectedQA === qa.id && qa.answer}
          </div>
        ))}
        <div className={styles.bottomText}>
          Join us and become part of a successful company where your
          contribution will be appreciated!
        </div>
      </div>
      <Image src={glass13} alt="glass13" className={styles.glass13} />
      <Image src={glass351} alt="glass351" className={styles.glass351} />
      {/*<Image src={blur} alt="blur" className={styles.blur} />*/}
    </div>
  );
};

export default QA;
