import { IQAData } from "@/components/designersLanding/components/qa";
import styles from "./styles.module.scss";

export const qaDataStrategists: IQAData[] = [
  {
    id: 1,
    question:
      "What level of experience and skills are required of candidates for a QA tester position in your company?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          We are looking for candidates with one to three years of software
          testing experience. They should have good skills in analysing,
          documenting bugs and experience with different types of testing.
          Candidates with knowledge of testing tools and methods, experience
          with databases and version control tools are also welcome.
        </div>
      </div>
    ),
  },
  {
    id: 2,
    question:
      "How do you ensure continuous development and training of your QA specialists?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          <span>
            We strive to create an environment in which our employees can grow
            and develop in their professional field, as well as expand their
            knowledge and skills in other areas.
          </span>
        </div>
        <div className={styles.text}>
          To this end, we offer various training programmes and courses, both
          internally and externally. Our quality assurance specialists have
          access to online courses and webinars to help them expand their
          knowledge of quality and project management.
        </div>
        <div className={styles.text}>
          An important aspect of our approach to training is to create
          conditions for the professional growth of our employees.{" "}
          <span>
            We provide opportunities for career growth and development, and
            encourage participation in projects that allow our specialists to
            broaden their horizons and acquire new skills.
          </span>
        </div>
      </div>
    ),
  },
  {
    id: 3,
    question:
      "What is your company's process for evaluating and monitoring the quality of QA work?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          <span>
            The QA process begins with analysing test results and identifying
            problems.
          </span>
          We then develop a corrective action plan, which includes defining
          tasks and deadlines, as well as assigning responsibilities to team
          members.
        </div>
        <div className={styles.text}>
          Once tasks are defined, we begin the QA process, which includes
          checking the quality of tasks, analysing test results and evaluating
          the performance of QA specialists. This process may include both
          automated and manual QA methods.
        </div>
        <div className={styles.text}>
          At the end of each stage, we analyse the data obtained and determine
          what actions need to be taken to improve the quality of the work. If
          problems are identified, we develop corrective actions and incorporate
          them into the quality control process.
        </div>
      </div>
    ),
  },
  {
    id: 4,
    question:
      "How do you monitor and ensure the quality of work at each stage of product development?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          <span>At Djosu, quality control is a key priority. </span>
        </div>
        <div className={styles.text}>
          We realise that the quality of our work directly affects the
          satisfaction of our customers and the success of our business.
          Therefore, we have developed a quality control system that covers all
          stages of product development.
        </div>
        <ul>
          <li>
            <span>During the planning phase,</span>
            we define quality requirements and develop standards that our
            product must meet. We also conduct risk analyses to identify
            potential problems and take corrective action.
          </li>

          <li>
            <span>During the development phase,</span>
            we use various testing methods such as functional testing, load
            testing, security testing, etc. This allows us to identify errors
            and defects at the early stages of development, which reduces the
            cost of fixing them in the future.
          </li>
          <li>
            <span>At the implementation stage,</span>
            we perform quality control at all stages of the process, from
            software development to hardware installation and configuration. We
            also monitor system performance in real time to identify and correct
            potential problems in a timely manner.
          </li>
          <li>
            <span>Finally, at the support stage,</span>
            we constantly monitor the quality of our product and promptly
            respond to all user requests and complaints. We also regularly audit
            the system to ensure that it meets the established quality
            standards.
          </li>
        </ul>
      </div>
    ),
  },
  {
    id: 5,
    question:
      "Do you consider hiring external QA experts to solve complex problems or conduct quality audits?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          In some cases, we may engage external QA specialists to perform audits
          or resolve complex issues, but we prefer to utilise our internal
          resources to ensure process control and quality of work.
        </div>
      </div>
    ),
  },
];
