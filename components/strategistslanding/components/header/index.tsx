"use client";
import React, { FC } from "react";

import menu from "@/public/assets/designers-icons/header/menu.svg";
import logo from "@/public/assets/designers-icons/header/logo.png";
import glass from "@/public/assets/strategists-icons/header/glass.png";
import cylinder from "@/public/assets/strategists-icons/header/cylinder.png";
import spiral from "@/public/assets/strategists-icons/header/spiral.png";
import shape from "@/public/assets/strategists-icons/header/shape.png";
import become from "@/public/assets/strategists-icons/header/become.svg";

import styles from "./styles.module.scss";
import Image from "next/image";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import cn from "classnames";
import Menu from "@/components/lips/menu";

interface IHeaderProps {
  setSelectedRef: (id: number) => void;
}

const Header: FC<IHeaderProps> = ({ setSelectedRef }) => {
  const { isMobile } = useResizeWindow();

  const handleFeedbackClick = () => {
    setSelectedRef(0);
  };

  const handleAboutUsClick = () => {
    setSelectedRef(1);
  };

  const handleVacanciesClick = () => {
    setSelectedRef(2);
  };

  const handleBenefitsClick = () => {
    setSelectedRef(3);
  };

  return (
    <div className={styles.wrapper}>
      <Menu
        setSelectedRef={setSelectedRef}
        className={styles.menu}
        classNameBurgerMenu={styles.burgerMenu}
      />
      <div className={styles.title1}>
        Social <span> Media </span> <br /> Strategists
      </div>
      <div className={styles.title2}>social media wizards!</div>
      <div className={styles.extraMenu}>
        <button className={styles.btn1} onClick={handleFeedbackClick}>
          Vacancies
        </button>
        <button className={styles.btn2} onClick={handleAboutUsClick}>
          Benefits
        </button>
        <button className={styles.btn3} onClick={handleVacanciesClick}>
          Feedback
        </button>
        <button className={styles.btn4} onClick={handleBenefitsClick}>
          Q&A
        </button>
      </div>

      <div className={cn(styles.extraMenu2, styles.extraMenu)}>
        <div className={styles.btn1}>Feedback</div>
        <div className={styles.btn2}>Benefits</div>
        <div className={styles.btn3}>Vacancies</div>
      </div>
      <div className={styles.glass}>
        <Image src={glass} alt="glass" />
      </div>
      <Image src={spiral} alt="spiral" className={styles.spiral} />
      <Image src={become} alt="become" className={styles.become} />
      <Image src={cylinder} alt="spiral" className={styles.cylinder} />
      <Image src={shape} alt="shape" className={styles.shapeRight} />
      <Image src={shape} alt="shapeLeft" className={styles.shapeLeft} />
    </div>
  );
};
export default Header;
