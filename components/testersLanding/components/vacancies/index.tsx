import React, { FC } from "react";

import cylinder from "@/public/assets/testers-icons/vacancies/cylinder.png";
import glass from "@/public/assets/icons/glass-354.png";
import shape from "@/public/assets/testers-icons/vacancies/shape.png";
import roundCube from "@/public/assets/testers-icons/vacancies/roundCube.png";

import Image from "next/image";

import styles from "./styles.module.scss";
import cn from "classnames";
import CustomButton from "@/components/lips/customButton";

const Vacancies: FC = () => {
  return (
    <div className={styles.containerVacancies}>
      <div className={cn(styles.text, styles.text1)}>
        For many years now, <span>Djosu</span> has been considered one of{" "}
        <span>the best in the information technology industry.</span>
      </div>
      <Image src={roundCube} alt="roundCube" className={styles.roundCube} />
      <div className={styles.text2}>
        <div className={styles.tx1}>In our long time in the market, </div>
        <div className={styles.tx2}>we have been able to help many people.</div>
        <div className={styles.tx3}>We can help you too!</div>
      </div>
      <div className={cn(styles.text, styles.text3)}>
        Join us and become part of a successful company where your contribution
        will be appreciated!
      </div>
      <div className={styles.text4}>QA Tester</div>
      <div className={styles.text5}>
        <div className={styles.tx51}>
          Quality control of developed products and analysis of possible errors
          in end users.
        </div>
        <div className={styles.tx52}>
          Development of test suites and their regular testing.
        </div>
        <div className={styles.tx53}>
          Preparation of test data and writing test methods.
        </div>
      </div>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <Image src={cylinder} alt="cylinder" className={styles.cylinder} />
      <Image src={glass} alt="glass" className={styles.glass} />
      <div className={styles.shape}>
        <Image src={shape} alt="shape" />
      </div>
    </div>
  );
};

export default Vacancies;
