"use client";
import { FC } from "react";

import menu from "@/public/assets/designers-icons/header/menu.svg";
import logo from "@/public/assets/designers-icons/header/logo.png";
import cube from "@/public/assets/testers-icons/header/cube.png";
import roundCube from "@/public/assets/testers-icons/header/round-cube.png";

import styles from "./styles.module.scss";
import Image from "next/image";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import cn from "classnames";
import Menu from "@/components/lips/menu";

interface IHeaderProps {
  setSelectedRef: (id: number) => void;
}

const Header: FC<IHeaderProps> = ({ setSelectedRef }) => {
  const { isMobile } = useResizeWindow();

  const handleFeedbackClick = () => {
    setSelectedRef(0);
  };

  const handleAboutUsClick = () => {
    setSelectedRef(1);
  };

  const handleVacanciesClick = () => {
    setSelectedRef(2);
  };

  const handleBenefitsClick = () => {
    setSelectedRef(3);
  };

  return (
    <div className={styles.wrapper}>
      <Menu
        setSelectedRef={setSelectedRef}
        className={styles.menu}
        classNameBurgerMenu={styles.burgerMenu}
      />

      <div className={styles.title1}>
        <div className={styles.text1}>Unlock the world</div>
      </div>

      <div className={styles.title2}>
        <div className={styles.text2}>of internet with us!</div>
      </div>

      <div className={styles.extraMenu}>
        <button className={styles.btn1} onClick={handleFeedbackClick}>
          Vacancies
        </button>
        <button className={styles.btn2} onClick={handleAboutUsClick}>
          Benefits
        </button>
        <button className={styles.btn3} onClick={handleVacanciesClick}>
          Feedback
        </button>
        <button className={styles.btn4} onClick={handleBenefitsClick}>
          Q&A
        </button>
      </div>
      <div className={styles.blur} />

      <Image src={cube} alt="cube" className={styles.cube} />
      <Image src={roundCube} alt="roundCube" className={styles.roundCube} />
    </div>
  );
};
export default Header;
