import React, { FC, useRef, useState } from "react";
import photo1 from "@/public/assets/testers-icons/feedback/photo1.png";
import photo2 from "@/public/assets/testers-icons/feedback/photo2.png";
import photo3 from "@/public/assets/testers-icons/feedback/photo3.png";
import circlyPhoto from "@/public/assets/testers-icons/feedback/circly-photo2.png";
import cubePhoto from "@/public/assets/testers-icons/feedback/cube-photo3.png";
import pillPhoto from "@/public/assets/testers-icons/feedback/pill-photo1.png";
import line from "@/public/assets/testers-icons/feedback/line.svg";
import roundCube from "@/public/assets/testers-icons/feedback/roundCube.png";
import pill from "@/public/assets/testers-icons/feedback/pill.png";
import sphere from "@/public/assets/testers-icons/feedback/sphere.png";
import Image from "next/image";

import styles from "./styles.module.scss";
import cn from "classnames";
import CustomButton from "@/components/lips/customButton";

const Feedback: FC = () => {
  return (
    <div className={styles.containerFeedback}>
      <div className={styles.feedback1}>
        <div className={styles.photo}>
          <Image src={pillPhoto} alt="pillPhoto" className={styles.pillPhoto} />
          <Image src={photo1} alt="photo1" className={styles.photo1} />
        </div>
        <div className={styles.name}>Leyton Orwig</div>
        <Image src={line} alt="line" className={styles.line} />
        <div className={cn(styles.text, styles.text1)}>
          I work as a QA tester at Djosu and I would like to share my
          impressions.
        </div>
        <div className={cn(styles.text, styles.text2)}>
          Here I feel part of a big and friendly team, where everyone is ready
          to help and support.
        </div>
      </div>
      <div className={cn(styles.feedback2, styles.feedback1)}>
        <div className={styles.photo}>
          <Image src={photo2} alt="photo1" className={styles.photo1} />
          <Image
            src={circlyPhoto}
            alt="pillPhoto"
            className={styles.pillPhoto}
          />
        </div>
        <div className={styles.name}>Jubin Severson</div>
        <Image src={line} alt="line" className={styles.line} />
        <div className={cn(styles.text, styles.text1)}>
          Djosu values quality and strives to provide customers with the best
          services.
        </div>
        <div className={cn(styles.text, styles.text2)}>
          This is also reflected in our work: we are constantly improving our
          skills and following new trends in testing.
        </div>
        <div className={cn(styles.text, styles.text3)}>
          I recommend Djosu to everyone who wants to work in a team of
          professionals and contribute to the development of quality software.
        </div>
      </div>
      <div className={cn(styles.feedback3, styles.feedback1)}>
        <div className={styles.photo}>
          <Image src={cubePhoto} alt="pillPhoto" className={styles.pillPhoto} />
          <Image src={photo3} alt="photo1" className={styles.photo1} />
        </div>
        <div className={styles.name}>Stefano Vogal</div>
        <Image src={line} alt="line" className={styles.line} />
        <div className={cn(styles.text, styles.text1)}>
          I have been working as a QA tester at Djosu for a few months now and I
          am very happy with my choice.
        </div>
        <div className={cn(styles.text, styles.text2)}>
          During this time, I have managed to get acquainted with wonderful
          colleagues who are always ready to help and support.
        </div>
        <div className={cn(styles.text, styles.text3)}>
          During this time, I have managed to get acquainted with wonderful
          colleagues who are always ready to help and support.
        </div>
      </div>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <Image src={roundCube} alt="roundCube" className={styles.roundCube} />
      <Image src={pill} alt="pill" className={styles.pill} />
      <Image src={sphere} alt="sphere" className={styles.sphere} />
      <div className={styles.blurLeft} />
      <div className={styles.blurRight} />
    </div>
  );
};

export default Feedback;
