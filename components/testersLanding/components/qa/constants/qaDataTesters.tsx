import { IQAData } from "@/components/designersLanding/components/qa";
import styles from "./styles.module.scss";

export const qaDataTesters: IQAData[] = [
  {
    id: 1,
    question:
      "What level of experience and skills are required of candidates for a QA tester position in your company?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          <span>
            Djosu always endeavours to hire only the best people in their field.
            We value the experience and skills of our QA testers, and we expect
            candidates to have a certain level of expertise.
          </span>
        </div>
        <div className={styles.text}>
          We are looking for candidates with one to year of software testing
          experience. It is important that the candidate has good bug analysis
          and documentation skills, as well as experience with different types
          of testing such as functional, regression and load testing.
        </div>
        <div className={styles.text}>
          In addition, we welcome candidates who are familiar with various
          testing tools and techniques, including automated testing and
          performance testing. Knowledge of database fundamentals and experience
          with version control tools would also be a plus.
        </div>
      </div>
    ),
  },
  {
    id: 2,
    question:
      "How do you ensure continuous development and training of your QA specialists?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          <span>
            At Djosu, we are constantly striving to develop and train our
            specialists.
          </span>
        </div>
        <div className={styles.text}>
          We offer various courses and trainings to help our employees improve
          their skills and knowledge.
        </div>
        <div className={styles.text}>
          In addition, we regularly organise seminars and conferences where our
          specialists can share their experience with colleagues from other
          companies. We also offer opportunities to participate in international
          projects, which allows our employees to broaden their horizons and
          acquire new knowledge.
        </div>
      </div>
    ),
  },
  {
    id: 3,
    question:
      "What is your company's process for evaluating and monitoring the quality of QA work?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          The process of QA evaluation and quality control involves several
          steps.
        </div>
        <ol>
          <li>
            Each project is evaluated at the initial stage to determine the main
            requirements and objectives.
          </li>
          <li>
            Then testing is conducted, which includes functional, load and
            regression testing.
          </li>
          <li>
            The test results are then analysed and bugs and errors are
            identified.
          </li>
          <li>
            Next, quality control is performed, which includes checking all the
            bugs found and fixing them.
          </li>
          <li>
            Finally, final testing is performed, which confirms that all tasks
            have been completed and the product is ready for release.
          </li>
        </ol>
      </div>
    ),
  },
  {
    id: 4,
    question:
      "How do you monitor and ensure the quality of work at each stage of product development?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          At Djosu, the quality of work is controlled at all stages of product
          development. We assess requirements and define project goals, then we
          test, analyse results and fix bugs. After that, we perform final
          testing and release the product, ensuring its quality at every stage.
        </div>
      </div>
    ),
  },
  {
    id: 5,
    question:
      "Do you consider hiring external QA experts to solve complex problems or conduct quality audits?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          Yes, we may consider hiring external QA specialists to solve complex
          problems or conduct audits. However, we prefer to use internal
          resources, as our specialists are more familiar with the specifics of
          the company&apos;s work.
        </div>
      </div>
    ),
  },
];
