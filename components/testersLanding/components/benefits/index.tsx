import React, { FC } from "react";
import face from "@/public/assets/testers-icons/benefits/face.png";
import pill from "@/public/assets/testers-icons/benefits/pill.png";
import djosuMobile from "@/public/assets/testers-icons/benefits/djosu-mobile.png";
import Image from "next/image";

import styles from "./styles.module.scss";
import cn from "classnames";
import CustomButton from "../../../lips/customButton";
import DjosuText from "../../../lips/djosuText";

const Benefits: FC = () => {
  return (
    <div className={styles.containerBenefits}>
      <div className={styles.title}>
        <div className={styles.tx1}>
          <span>The company </span>is committed to{" "}
          <span>helping you find ideal job</span>
        </div>
        <div className={styles.tx2}>
          And providing employers with <span>quality candidates.</span>
        </div>
      </div>
      <div className={styles.benefits}>
        <div className={styles.face}>
          <Image src={face} alt="glassLeft" />
        </div>
        <div className={styles.schedule}>
          Flexible working
          <br />
          schedule
        </div>
        <div className={styles.salary}>High salary</div>
        <div className={styles.bonuses}>Pleasant bonuses</div>
        <div className={styles.opportunity}>Development opportunity</div>
      </div>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <Image src={pill} alt="pill" className={styles.pill} />
      <Image
        src={djosuMobile}
        alt="djosuMobile"
        className={styles.djosuMobile}
      />
    </div>
  );
};

export default Benefits;
