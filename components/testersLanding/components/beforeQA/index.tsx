import { FC } from "react";

import arrow from "@/public/assets/developers-icons/beforeQA/arrow.png";
import arrowMobile from "@/public/assets/developers-icons/beforeQA/arrow-mobile.png";
import glass from "@/public/assets/developers-icons/beforeQA/glass.png";

import Image from "next/image";

import styles from "./styles.module.scss";
import CustomButton from "@/components/lips/customButton";

const BeforeQA: FC = () => {
  return (
    <div className={styles.containerBeforeQA}>
      <div className={styles.text1}>
        Djosu is an innovative IT company specialising in the development and
        implementation of cutting-edge technologies.
      </div>{" "}
      <div className={styles.text2}>
        We offer a wide range of services including website development, mobile
        app development, systems integration and more.
      </div>
      <div className={styles.text3}>
        Djosu is your reliable partner in the world of information technology,
        which will ensure your success and development of your career
      </div>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <Image src={glass} alt="glass" className={styles.glass} />
      <Image src={arrow} alt="arrow" className={styles.arrow} />
      <Image
        src={arrowMobile}
        alt="arrowMobile"
        className={styles.arrowMobile}
      />
      <div className={styles.blur} />
    </div>
  );
};

export default BeforeQA;
