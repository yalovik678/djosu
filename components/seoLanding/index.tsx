"use client";

import AboutUs from "@/components/commonLanding/aboutUs";
import { FC, RefObject, useEffect, useRef, useState } from "react";

import Header from "@/components/seoLanding/componens/header";
import Vacancies from "@/components/seoLanding/componens/vacancies";
import Benefits from "./componens/benefits";
import Feedback from "@/components/seoLanding/componens/feedback";
import QA from "@/components/seoLanding/componens/qa";

export const SEOLanding: FC = () => {
  const headerRef = useRef<HTMLDivElement>(null);
  const feedbackRef = useRef<HTMLDivElement>(null);
  const QARef = useRef<HTMLDivElement>(null);
  const vacanciesRef = useRef<HTMLDivElement>(null);
  const benefitsRef = useRef<HTMLDivElement>(null);
  const [selectedRef, setSelectedRef] =
    useState<RefObject<HTMLDivElement>>(headerRef);

  useEffect(() => {
    if (selectedRef) {
      selectedRef.current?.scrollIntoView({ behavior: "smooth" });
    }
  }, [selectedRef]);

  const handleSetRef = (id: number) => {
    switch (id) {
      case 0:
        setSelectedRef(vacanciesRef);
        break;
      case 1:
        setSelectedRef(benefitsRef);
        break;
      case 2:
        setSelectedRef(feedbackRef);
        break;
      case 3:
        setSelectedRef(QARef);
        break;
      default:
        console.log("here");
        break;
    }
  };

  return (
    <div>
      {/*<div ref={headerRef}>*/}
      <Header setSelectedRef={handleSetRef} />
      {/*</div>*/}
      <div ref={vacanciesRef}>
        <Vacancies />
      </div>

      <div ref={benefitsRef}>
        <Benefits />
      </div>

      <div ref={feedbackRef}>
        <Feedback />
      </div>
      <div ref={QARef}>
        <QA />
      </div>

      <AboutUs />
    </div>
  );
};
