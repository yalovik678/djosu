import React, { FC } from "react";
import liquida from "@/public/assets/icons/mujer-liquida-3.png";
import blur from "@/public/assets/seo-icons/vacancy/blur.svg";
import djosu from "@/public/assets/seo-icons/vacancy/djosu.png";

import Image from "next/image";

import styles from "./styles.module.scss";
import cn from "classnames";

const Vacancies: FC = () => {
  return (
    <div className={styles.containerVacancies}>
      <div className={styles.textTop}>
        <div>
          We offer <span>unique opportunities</span> for{" "}
          <span>IT professionals</span> who are looking for not only a job,
        </div>
        <div className={styles.tx2}>
          but also the chance to realize their potential and be part of
          something bigger.
        </div>
      </div>
      <div className={cn(styles.vacancy, styles.copywriter)}>
        <div className={styles.title}>SEO copywriter</div>
        <div className={styles.count}>+31 vacancies</div>
        <div className={styles.respond}>Respond</div>
      </div>
      <div className={cn(styles.vacancy, styles.analyst)}>
        <div className={styles.title}>SEO analyst</div>
        <div className={styles.count}>
          +24 <br /> vacancies
        </div>
        <div className={styles.respond}>Respond</div>
      </div>
      <div className={cn(styles.vacancy, styles.manager)}>
        <div className={styles.title}>
          SEO <br /> content <br /> manager
        </div>
        <div className={styles.count}>+10 vacancies</div>
        <div className={styles.respond}>Respond</div>
      </div>
      <div className={styles.textBottom}>
        <div className={styles.tx1}>
          With us you will find not only <span> interesting projects,</span>
        </div>
        <div className={styles.tx2}>
          but also <span> opportunities to learn and develop,</span> participate
          in international conferences and workshops.
        </div>
      </div>
      <Image src={liquida} alt="liquida" className={styles.liquida} />
      <Image src={blur} alt="blur" className={styles.blur} />
      <Image src={djosu} alt="djosu" className={styles.djosu} />
      <div className={styles.blurRight} />
    </div>
  );
};

export default Vacancies;
