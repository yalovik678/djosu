import React, { FC } from "react";
import cube from "@/public/assets/seo-icons/header/cube.png";
import glass from "@/public/assets/seo-icons/benefits/glass.png";
import Image from "next/image";

import styles from "./styles.module.scss";
import cn from "classnames";
import CustomButton from "../../../lips/customButton";

const Benefits: FC = () => {
  return (
    <div className={styles.containerBenefits}>
      <div className={styles.title}>
        <div className={styles.tx1}>In our long time in the market, </div>
        <div className={styles.tx2}>we have been able to help many people.</div>
        <div className={styles.tx3}>We can help you too!</div>
      </div>
      <div className={cn(styles.benefit, styles.schedule)}>
        Flexible working schedule
      </div>
      <div className={cn(styles.benefit, styles.salary)}>High salary</div>
      <div className={cn(styles.benefit, styles.bonuses)}>Pleasant bonuses</div>
      <div className={cn(styles.benefit, styles.development)}>
        Development opportunity
      </div>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <Image src={cube} alt="cube" className={styles.cube} />
      <Image src={glass} alt="glassLeft" className={styles.glassLeft} />
      <Image src={glass} alt="glassLeft" className={styles.glassCenter} />
      <Image src={glass} alt="glassLeft" className={styles.glassRight} />
      <div className={styles.blur} />
    </div>
  );
};

export default Benefits;
