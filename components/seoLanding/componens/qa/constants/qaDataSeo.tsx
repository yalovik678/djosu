import { IQAData } from "@/components/designersLanding/components/qa";
import styles from "./styles.module.scss";

export const qaDataSeo: IQAData[] = [
  {
    id: 1,
    question:
      "How does the company treat external expertise and cooperation with agencies to solve SEO tasks?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          As an SEO specialist at Djosu, you are the maestro of the digital
          orchestra, conducting a symphony of keywords and meta tags to create a
          masterpiece of online visibility. Your role is not just a job; it’s a
          quest to elevate Djosu’s digital footprint to legendary status.
        </div>
        <div className={styles.text}>
          You will navigate the vast seas of search engine algorithms, charting
          courses through keyword research and competitive analysis.
        </div>
        <div className={styles.text}>
          You’ll delve into the technical depths, from overseeing crawling and
          indexing to implementing schema markup. Your strategic insights will
          drive content development, aligning with our SEO goals to captivate
          and engage our audience.
        </div>
        <div className={styles.text}>
          At Djosu, your creativity knows no bounds. You will collaborate with
          content creators, weaving SEO magic into every paragraph.
        </div>
        <div className={styles.text}>
          <span>
            At Djosu, we don’t just follow trends—we set them. Your journey as
            an SEO specialist will be epic, filled with challenges, triumphs,
            and the joy of pushing boundaries.
          </span>
        </div>
      </div>
    ),
  },
  {
    id: 2,
    question:
      "Does the company expect training and development for its SEO specialists or do they have to keep up with industry changes on their own?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          <span>
            We don’t just anticipate change; we embrace it, and we ensure that
            our specialists are equipped to ride the wave of industry
            transformations with confidence and expertise.
          </span>
        </div>
        <div className={styles.text}>
          We believe in empowering our SEO specialists with a treasure trove of
          knowledge and skills.
        </div>
        <div className={styles.text}>
          Our comprehensive training programs are designed to keep you at the
          cutting edge of SEO practices. From in-depth workshops to advanced
          certification courses, we provide the resources you need to stay ahead
          of the curve.
        </div>
        <div className={styles.text}>
          We have a proactive approach to staying abreast of industry shifts,
          whether it’s the latest Google algorithm update or the newest trends
          in digital marketing. Our team is always on the pulse, ensuring that
          you have the insights and tools to adapt swiftly and effectively.
        </div>
        <div className={styles.text}>
          We foster an environment where knowledge is shared freely, questions
          are encouraged, and innovation is the status quo. Whether through
          peer-to-peer learning or mentorship programs, we support your
          professional journey every step of the way.
        </div>
        <div className={styles.text}>
          <span>Djosu is your launchpad.</span>
        </div>
      </div>
    ),
  },
  {
    id: 3,
    question:
      "What tasks will the SEO specialist have to solve in the company and how well do they fit his/her profile?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          Step into the world of Djosu, where the role of an SEO specialist is
          not just a job, but a grand adventure in the digital realm. At Djosu,
          we understand that the landscape of search engine optimization is a
          vast and ever-changing wilderness, and our SEO specialists are the
          intrepid explorers charting the course to visibility and success.
        </div>
        <div className={styles.text}>
          As an SEO specialist at Djosu, your quest is to conquer the summit of
          search rankings. Your arsenal will be filled with:
        </div>
        <ul>
          <li>tools of keyword research,</li>
          <li>strategy of competitive analysis,</li>
          <li>craft of content optimization,</li>
          <li>wield the power of XML sitemaps.</li>
        </ul>
        <div className={styles.text}>
          Your profile as an SEO specialist is a tapestry of skills woven from
          the threads of performance marketing, conversion optimization, and
          customer acquisition.
        </div>
        <div className={styles.text}>
          At Djosu, we don’t just offer tasks—we offer epic tales of digital
          triumph. Your role will be pivotal in the narrative of our company’s
          success, as you forge alliances with content creators, developers, and
          marketing strategists to create a cohesive and powerful online
          presence.
        </div>
        <div className={styles.text}>
          If you’re ready to embark on this quest, to bring your expertise and
          passion to a company that values innovation and creativity, then Djosu
          is your destined path.
        </div>
      </div>
    ),
  },
  {
    id: 4,
    question:
      "What KPIs are set for the SEO specialist and how is his/her performance evaluated?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          At Djosu, we believe in the power of visibility and precision. Our SEO
          specialists are the maestros of the digital orchestra, ensuring that
          our brand’s presence resonates across the vast expanse of the
          internet. Here’s how we set the stage for their success and evaluate
          their virtuoso performances:
        </div>
        <ul>
          <li>
            <span>Search Visibility: </span>
          </li>
          <div className={styles.notLeft}>
            Like a spotlight on a lead performer, search visibility ensures our
            brand shines brightest when and where it matters.
          </div>
          <li>
            <span>Conversions from Organic Traffic:</span>
          </li>
          <div className={styles.notLeft}>
            Every click is an audience member, and every conversion is a
            standing ovation. We track these meticulously to understand the
            impact of our SEO strategies.
          </div>
          <li>
            <span>Keyword Rankings:</span>
          </li>
          <div className={styles.notLeft}>
            Each keyword is a note in our symphony, and we aim to hit the high
            notes, ranking at the top of search engine charts.
          </div>
          <li>
            <span>Click-Through Rate (CTR): </span>
          </li>
          <div className={styles.notLeft}>
            This metric tells us if our audience finds our content compelling
            enough to take the next step.
          </div>
          <li>
            <span>Customer Lifetime Value (CLV): </span>
          </li>
          <div className={styles.notLeft}>
            We look beyond the first act, focusing on the long-term relationship
            with our audience, measuring the ongoing value they bring.
          </div>
          <li>
            <span>Cost Per Acquisition (CPA): </span>
          </li>
          <div className={styles.notLeft}>
            Every marketing move has a cost, and we strive for a performance
            that’s as cost-effective as it is captivating.
          </div>
          <li>
            <span>Evaluating Performance: </span>
          </li>
          <div className={styles.notLeft}>
            Our SEO specialists’ performances are appraised through a blend of
            analytics and artistry. We don’t just count the applause; we listen
            to its sincerity.
          </div>
          <div className={styles.notLeft}>
            Here’s how we measure the encore-worthy efforts:
          </div>
          <li>
            <span>Organic Traffic Analysis,</span>
          </li>
          <li>
            <span>Conversion Tracking, </span>
          </li>
          <li>
            <span>Keyword Position Tracking, </span>
          </li>
          <li>
            <span>Competitor Benchmarking.</span>
          </li>
        </ul>
        <div className={styles.text}>
          <span>
            At Djosu, we’re not just offering a job; we’re offering a stage for
            you to showcase your talent, where your performance will be seen,
            heard, and celebrated.
          </span>
        </div>
      </div>
    ),
  },
  {
    id: 5,
    question:
      "What tools and technologies are used in the company to work with SEO and how " +
      "do they correlate with generally accepted industry standards?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          Step into the world of Djosu, where the role of an SEO specialist is
          not just a job, but a grand adventure in the digital realm. At Djosu,
          we understand that the landscape of search engine optimization is a
          vast and ever-changing wilderness, and our SEO specialists are the
          intrepid explorers charting the course to visibility and success.
        </div>
        <div className={styles.text}>
          As an SEO specialist at Djosu, your quest is to conquer the summit of
          search rankings. Your arsenal will be filled with:
        </div>
        <ul>
          <li>
            <span>tools of keyword research,</span>
          </li>

          <li>
            <span>strategy of competitive analysis,</span>
          </li>

          <li>
            <span>craft of content optimization,</span>
          </li>

          <li>
            <span>craft of content optimization,</span>
          </li>
        </ul>
        <div className={styles.text}>
          Your profile as an SEO specialist is a tapestry of skills woven from
          the threads of performance marketing, conversion optimization, and
          customer acquisition.
        </div>
        <div className={styles.text}>
          At Djosu, we don’t just offer tasks—we offer epic tales of digital
          triumph. Your role will be pivotal in the narrative of our company’s
          success, as you forge alliances with content creators, developers, and
          marketing strategists to create a cohesive and powerful online
          presence.
        </div>
        <div className={styles.text}>
          <span>
            If you’re ready to embark on this quest, to bring your expertise and
            passion to a company that values innovation and creativity, then
            Djosu is your destined path.
          </span>
        </div>
      </div>
    ),
  },
];
