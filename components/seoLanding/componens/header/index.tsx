"use client";
import { FC } from "react";

import menu from "@/public/assets/designers-icons/header/menu.svg";
import logo from "@/public/assets/designers-icons/header/logo.png";
import cube from "@/public/assets/seo-icons/header/cube.png";
import branch from "@/public/assets/seo-icons/header/branch.png";

import styles from "./styles.module.scss";
import Image from "next/image";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import cn from "classnames";
import Menu from "@/components/lips/menu";

interface IHeaderProps {
  setSelectedRef: (id: number) => void;
}

const Header: FC<IHeaderProps> = ({ setSelectedRef }) => {
  const { isMobile } = useResizeWindow();

  const handleFeedbackClick = () => {
    setSelectedRef(0);
  };

  const handleAboutUsClick = () => {
    setSelectedRef(1);
  };

  const handleVacanciesClick = () => {
    setSelectedRef(2);
  };

  const handleBenefitsClick = () => {
    setSelectedRef(3);
  };

  return (
    <div className={styles.wrapper}>
      <Menu
        setSelectedRef={setSelectedRef}
        className={styles.menu}
        classNameBurgerMenu={styles.burgerMenu}
      />

      <div className={styles.title}>
        <div className={styles.text1}>Unlock the world </div>
        <div className={styles.text2}>of internet with us!</div>
      </div>

      <button
        className={cn(styles.btnExtra, styles.btnVacancies)}
        onClick={handleFeedbackClick}
      >
        Vacancies
      </button>
      <button
        className={cn(styles.btnExtra, styles.btnBenefits)}
        onClick={handleAboutUsClick}
      >
        Benefits
      </button>
      <button
        className={cn(styles.btnExtra, styles.btnFeedback)}
        onClick={handleVacanciesClick}
      >
        Feedback
      </button>
      <button
        className={cn(styles.btnExtra, styles.btnQA)}
        onClick={handleBenefitsClick}
      >
        Q&A
      </button>

      <Image src={cube} alt="cube" className={styles.cube} />
      <Image src={branch} alt="branch" className={styles.branch} />
    </div>
  );
};
export default Header;
