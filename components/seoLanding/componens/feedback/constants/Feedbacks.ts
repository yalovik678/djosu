import photo2 from "@/public/assets/developers-icons/feedback/photo-2.png";
import photo3 from "@/public/assets/developers-icons/feedback/photo-3.png";
import photo1 from "@/public/assets/developers-icons/feedback/photo-1.png";
import { IFeedbacks } from "@/components/developersLanding/components/feedback/components/sliderFeedbacks";

export const Feedbacks: IFeedbacks[] = [
  {
    id: 1,
    photo: photo1 as string,
    name: "Maneet Dinkins",
    feedback: [
      "I have been working with Djosu for a few months now and I would like to share my impressions.",
      "The company employs experienced developers, managers and customer support specialists. ",
      "They are always ready to help and answer questions.",
    ],
  },
  {
    id: 2,
    photo: photo2 as string,
    name: "Grady Resnick",
    feedback: [
      "The company offers different remuneration options, including a fixed rate, a percentage of sales and bonuses for achieving certain results. ",
      "This allows employees to feel confident and plan their finances.  ",
      "I am very satisfied with my work at Djosu.",
    ],
  },
  {
    id: 3,
    photo: photo3 as string,
    name: "Alekzander Vogal",
    feedback: [
      "I am delighted!",
      "Djosu's office has all the conditions for comfortable work: comfortable workstations, modern equipment and free coffee. ",
      "The company also regularly organises corporate events and trainings for employee development. ",
    ],
  },
];
