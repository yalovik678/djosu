import React, { FC, useRef, useState } from "react";
import photo2 from "@/public/assets/seo-icons/feedback/photo2.png";
import photo3 from "@/public/assets/seo-icons/feedback/photo3.png";
import glass from "@/public/assets/seo-icons/feedback/glass.png";
import branch from "@/public/assets/seo-icons/header/branch.png";
import photo1 from "@/public/assets/developers-icons/feedback/photo-1.png";
import Image from "next/image";

import styles from "./styles.module.scss";
import cn from "classnames";
import CustomButton from "@/components/lips/customButton";

const Feedback: FC = () => {
  return (
    <div className={styles.containerFeedback}>
      <div className={styles.feedbacks}>
        <div className={styles.feedback1}>
          <div className={styles.name}>Valery Quinn</div>
          <div className={styles.containerPhoto}>
            <div className={styles.textByPhoto}>
              I work as an SEO specialist at Djosu and I want to share the
              benefits of our company.
            </div>
            <div className={styles.photo}>
              <Image src={photo1} alt="photo1" />
              <div className={styles.blur} />
            </div>
          </div>
          <div className={styles.text1}>
            Individual approach to each client and taking into account the
            peculiarities of their business allows Djosu to develop unique
            promotion strategies that will be most effective for the client.{" "}
          </div>
        </div>
        <div className={cn(styles.feedback2, styles.feedback1)}>
          <div className={styles.name}>Mackenzy Schutz</div>
          <div className={styles.containerPhoto}>
            <div className={styles.photo}>
              <Image src={photo2} alt="photo1" />
              <div className={styles.blur} />
            </div>
            <div className={styles.textByPhoto}>
              I recommend Djosu to everyone who wants to receive quality service
              and achieve success in their field of activity.
            </div>
          </div>
          <div className={styles.text1}>
            Djosu has an atmosphere of co-operation and mutual support.
          </div>
          <div className={styles.text2}>
            Each employee contributes to the common cause, and this helps us to
            achieve high results.
          </div>
          <div className={styles.text3}>
            The management of the company always listens to the opinion of
            employees and strives to create a comfortable working environment.
          </div>
        </div>
        <div className={cn(styles.feedback3, styles.feedback1)}>
          <div className={styles.name}>Zennon Sweeney</div>
          <div className={styles.containerPhoto}>
            <div className={styles.textByPhoto}>
              Djosu is an excellent choice for those who are looking for a
              reliable partner in the field of SEO services.
            </div>
            <div className={styles.photo}>
              <Image src={photo3} alt="photo1" />
              <div className={styles.blur} />
            </div>
          </div>
          <div className={styles.text1}>
            Professionalism, individual approach and attention to detail make
            this company a leader in its field.
          </div>
          <div className={styles.text2}>
            Professionalism, individual approach and attention to detail make
            this company a leader in its field.
          </div>
          <div className={styles.text3}>
            This allows clients to be sure that their investment in SEO is
            justified and will bring the desired results.
          </div>
        </div>
      </div>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <Image src={glass} alt="glass" className={styles.glass} />
      <Image src={branch} alt="branch" className={styles.branch} />
      <div className={styles.blur} />
    </div>
  );
};

export default Feedback;
