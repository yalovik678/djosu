import { FC } from "react";
import baseImage from "@/public/assets/common-icons/beforeVacancies/base-image.png";
import springGreenLeft from "@/public/assets/common-icons/beforeVacancies/spring-green-right.png";
import springGreenRight from "@/public/assets/common-icons/beforeVacancies/spring-green-left.png";
import lineTop from "@/public/assets/common-icons/beforeVacancies/line-top.svg";
import lineTopMobile from "@/public/assets/common-icons/beforeVacancies/line-top-mobile.svg";
import lineBottom from "@/public/assets/common-icons/beforeVacancies/line-bottom.svg";
import ellipse from "@/public/assets/common-icons/beforeVacancies/ellipse.svg";
import vector from "@/public/assets/common-icons/beforeVacancies/vector.svg";

import Image from "next/image";

import styles from "./styles.module.scss";

const BeforeVacancies: FC = () => {
  return (
    <div className={styles.containerBeforeVacancies}>
      <div className={styles.text}>
        <div className={styles.tx1}>
          <span>The company </span>is committed to{" "}
          <span>helping you find ideal job</span>
        </div>
        <div className={styles.tx2}>
          And providing employers with <span>quality candidates.</span>
        </div>
      </div>
      <Image src={baseImage} alt="baseImage" className={styles.baseImage} />
      <Image
        src={springGreenRight}
        alt="springGreenRight"
        className={styles.springGreenRight}
      />
      <Image
        src={springGreenLeft}
        alt="springGreenLeft"
        className={styles.springGreenLeft}
      />
      <Image src={lineTopMobile} alt="lineTop" className={styles.lineTop} />
      <Image src={lineBottom} alt="lineBottom" className={styles.lineBottom} />
      <Image src={vector} alt="vector" className={styles.vector} />
      <Image src={ellipse} alt="ellipse" className={styles.ellipse} />
    </div>
  );
};

export default BeforeVacancies;
