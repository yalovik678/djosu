"use client";
import { FC } from "react";
import boxGreen from "@/public/assets/common-icons/vacancies/box-green.png";
import disk from "@/public/assets/common-icons/vacancies/disk-green.png";
import pyramid from "@/public/assets/common-icons/vacancies/pyramid-green.png";
import subtract from "@/public/assets/common-icons/vacancies/subtract.svg";
import subtractMobile from "@/public/assets/common-icons/vacancies/subtract-mobile.svg";
import star from "@/public/assets/common-icons/vacancies/star.svg";

import Image from "next/image";

import styles from "./styles.module.scss";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import Link from "next/link";

const Vacancies: FC = () => {
  const { isMobile } = useResizeWindow();
  return (
    <div className={styles.containerVacancies}>
      <div className={styles.text}>
        <div className={styles.tx1}>
          Djosu continues to develop and expand its services to help even more
          people{" "}
        </div>
        <div>find their dream IT jobs.</div>
      </div>
      <div className={styles.become}>Become part of our team now! </div>
      <Image src={boxGreen} alt="boxGreen" className={styles.boxGreen} />
      {!isMobile && <Image src={star} alt="star" className={styles.star} />}

      <Link href={"/designers"}>
        <div className={styles.oval}>
          <div className={styles.name}>Designers</div>
          <div className={styles.count}>+ 400</div>
        </div>
      </Link>

      <Link href={"/programmers"}>
        <div className={styles.rectangle}>
          <div className={styles.name}>Developers</div>
          <div className={styles.count}>+ 300</div>
        </div>
      </Link>

      <Link href={"/testers"}>
        <div className={styles.disk}>
          <Image src={disk} alt="disk" className={styles.image} />
          <div className={styles.name}>QA Tester</div>
          <div className={styles.count}>+204</div>
        </div>
      </Link>

      <Link href={"/strategists"}>
        <div className={styles.pyramid}>
          <Image src={pyramid} alt="pyramid" className={styles.image} />
          <div className={styles.name}>Social Media Strategist</div>
          <div className={styles.count}>+134</div>
        </div>
      </Link>

      <Link href={"/seo"}>
        <div className={styles.subtract}>
          {isMobile ? (
            <Image
              src={subtractMobile}
              alt="subtractMobile"
              className={styles.image}
            />
          ) : (
            <Image src={subtract} alt="subtract" className={styles.image} />
          )}

          <div className={styles.name}>SEO specialists</div>
          <div className={styles.count}>+350</div>
        </div>
      </Link>
    </div>
  );
};

export default Vacancies;
