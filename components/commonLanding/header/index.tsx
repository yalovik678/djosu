"use client";
import { FC, useState } from "react";

import figure1 from "@/public/assets/common-icons/header/figure1.png";
import figure2 from "@/public/assets/common-icons/header/figure2.png";
import figure3 from "@/public/assets/common-icons/header/figure3.png";
import figureBackground from "@/public/assets/common-icons/header/figure-background.png";
import figureBackgroundMobile from "@/public/assets/common-icons/header/figure-background-mobile.png";
import flowers from "@/public/assets/common-icons/header/flowers.svg";
import flowersMobile from "@/public/assets/common-icons/header/flower-mobile.svg";
import menu from "@/public/assets/common-icons/header/menu.svg";
import logo from "@/public/assets/common-icons/header/logo.png";

import styles from "./styles.module.scss";
import Image from "next/image";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import BurgerMenu from "@/components/lips/menu/components/burgerMenu";

interface IHeaderProps {
  setSelectedRef: (id: number) => void;
}

const Header: FC<IHeaderProps> = ({ setSelectedRef }) => {
  const { isMobile } = useResizeWindow();
  const [isOpenBurgerMenu, setIsOpenBurgerMenu] = useState(false);

  const handleFeedbackClick = () => {
    setSelectedRef(0);
  };

  const handleAboutUsClick = () => {
    setSelectedRef(1);
  };

  const handleVacanciesClick = () => {
    setSelectedRef(2);
  };

  const handleBenefitsClick = () => {
    setSelectedRef(3);
  };

  const handleClickBurgerMenu = () => {
    setIsOpenBurgerMenu((prevState) => !prevState);
  };

  const handleClickMenuItems = (id: number) => {
    switch (id) {
      case 0:
        setSelectedRef(2);
        break;
      case 1:
        setSelectedRef(3);
        break;
      case 2:
        setSelectedRef(0);
        break;
      case 3:
        setSelectedRef(1);
        break;
    }
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.overlay}>
        <Image src={figure1} alt="figure1" />
      </div>

      <div className={styles.overlay2}>
        <Image src={figure2} alt="figure2" />
      </div>

      <div className={styles.overlay3}>
        <Image src={figure3} alt="figure3" />
      </div>

      <div className={styles.overlayBackground}>
        {isMobile ? (
          <Image src={figureBackgroundMobile} alt="figureBackgroundMobile" />
        ) : (
          <Image src={figureBackground} alt="figureBackground" />
        )}
      </div>

      {isMobile ? (
        <Image
          src={flowersMobile}
          alt="flowersMobile"
          className={styles.flowers}
        />
      ) : (
        <Image src={flowers} alt="flowers" className={styles.flowers} />
      )}

      <div className={styles.menu}>
        {isMobile ? (
          <>
            <button
              onClick={() => {
                handleClickBurgerMenu();
              }}
            >
              <Image src={menu} alt="menu" className={styles.menuBurger} />
            </button>
            <Image src={logo} alt="logo" className={styles.logo} />
          </>
        ) : (
          <>
            <button className={styles.btn} onClick={handleFeedbackClick}>
              Feedback
            </button>
            <button className={styles.btn} onClick={handleAboutUsClick}>
              About us
            </button>
            <button className={styles.btn} onClick={handleVacanciesClick}>
              Vacancies
            </button>
            <button className={styles.btn} onClick={handleBenefitsClick}>
              Benefits
            </button>
          </>
        )}
      </div>
      {isOpenBurgerMenu && <BurgerMenu onClick={handleClickMenuItems} />}

      {isMobile && <div className={styles.welcome}>Welcome to Djosu.com</div>}

      <div className={styles.title}>
        <div className={styles.sp1}>start</div>
        <div className={styles.sp2}>career</div>
        <div className={styles.sp3}>now</div>
      </div>

      {isMobile && (
        <div className={styles.menuMobile}>
          <div className={styles.twoButtons1}>
            <button className={styles.btn1} onClick={handleFeedbackClick}>
              Feedback
            </button>
            <button className={styles.btn2} onClick={handleAboutUsClick}>
              About us
            </button>
          </div>
          <div className={styles.twoButtons2}>
            <button className={styles.btn1} onClick={handleVacanciesClick}>
              Vacancies
            </button>
            <button className={styles.btmDM} onClick={handleBenefitsClick}>
              Benefits
            </button>
          </div>
        </div>
      )}
    </div>
  );
};
export default Header;
