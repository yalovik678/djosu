"use client";
import React, { FC } from "react";

import instagram from "@/public/assets/common-icons/aboutUs/instagram.png";
import linkedIn from "@/public/assets/common-icons/aboutUs/linkedIn.png";
import twitter from "@/public/assets/common-icons/aboutUs/twitter.png";
import facebook from "@/public/assets/common-icons/aboutUs/facebook.png";
import plant from "@/public/assets/common-icons/aboutUs/strelitzia-plant.png";
import plantRight from "@/public/assets/common-icons/aboutUs/strelitzia-plant-right.png";
import golden from "@/public/assets/common-icons/aboutUs/golden.png";
import goldenMobile from "@/public/assets/common-icons/aboutUs/golden-mobile.png";
import spiral from "@/public/assets/common-icons/aboutUs/spiral.png";
import cone from "@/public/assets/common-icons/aboutUs/cone.png";
import substance from "@/public/assets/common-icons/aboutUs/substance.png";
import starBlack from "@/public/assets/common-icons/aboutUs/star-black.svg";
import starWhite from "@/public/assets/common-icons/aboutUs/star-white.svg";
import rock from "@/public/assets/icons/rock.png";

import Image from "next/image";

import styles from "./styles.module.scss";
import baseImage from "@/public/assets/common-icons/beforeQA/base-image.png";
import { useResizeWindow } from "../../../hooks/useResizeWindow";
import cn from "classnames";
import CustomButton from "@/components/lips/customButton";

interface IAboutUs {
  commonLanding: boolean;
}

const AboutUs: FC<IAboutUs> = ({ commonLanding = false }) => {
  const { isMobile } = useResizeWindow();
  return (
    <div
      className={cn(
        styles.containerAboutUs,
        commonLanding && styles.containerCommonLanding,
      )}
    >
      <div
        className={cn(styles.text, commonLanding && styles.textCommonLanding)}
      >
        Find us on social media!{" "}
      </div>

      <div className={styles.containerButtons}>
        <div className={styles.button}>
          <Image src={facebook} alt="Facebook" className={styles.icon} />
          <div className={styles.name}>Facebook</div>
        </div>
        <div className={styles.button}>
          <Image src={linkedIn} alt="LinkedIn" className={styles.icon} />
          <div className={styles.name}>LinkedIn</div>
        </div>
        <div className={styles.button}>
          <Image src={twitter} alt="Twitter" className={styles.icon} />
          <div className={styles.name}>Twitter</div>
        </div>
        <div className={styles.button}>
          <Image src={instagram} alt="Instagram" className={styles.icon} />
          <div className={styles.name}>Instagram</div>
        </div>
      </div>

      {/*{!commonLanding && (*/}
      {/*  <CustomButton*/}
      {/*    text="REGISTRATION"*/}
      {/*    className={styles.customButton}*/}
      {/*    onClick={() => console.log("here")}*/}
      {/*  />*/}
      {/*)}*/}

      <Image
        src={plant}
        alt="plant"
        className={cn(styles.plant, commonLanding && styles.plantCommonLanding)}
      />
      <Image src={plantRight} alt="plantRight" className={styles.plantRight} />
      {isMobile ? (
        <Image
          src={goldenMobile}
          alt="goldenMobile"
          className={styles.golden}
        />
      ) : (
        <div className={styles.golden}>
          <Image src={golden} alt="golden" />
        </div>
      )}

      <Image src={spiral} alt="spiral" className={styles.spiral} />
      <Image src={cone} alt="cone" className={styles.cone} />

      <Image
        src={starBlack}
        alt="starBlack"
        className={cn(
          styles.starBlack,
          commonLanding && styles.starBlackCommon,
        )}
      />
      <Image src={starWhite} alt="starWhite" className={styles.starWhite} />

      {!commonLanding && (
        <Image src={substance} alt="substance" className={styles.substance} />
      )}

      <Image src={rock} alt="rock" className={styles.rock} />
      <div className={styles.blurBottom} />
      <div className={styles.blurForCone} />
    </div>
  );
};

export default AboutUs;
