"use client";
import { FC } from "react";
import substanceRight from "@/public/assets/common-icons/beforeFeedback/substance-right.png";
import substanceLeft from "@/public/assets/common-icons/beforeFeedback/substance-left.png";
import group from "@/public/assets/common-icons/beforeFeedback/Group 370126.png";
import starWhite from "@/public/assets/common-icons/beforeFeedback/star-white.svg";
import starBlack from "@/public/assets/common-icons/beforeFeedback/star-black.png";
import icon1 from "@/public/assets/common-icons/beforeFeedback/icon1.svg";
import icon2 from "@/public/assets/common-icons/beforeFeedback/icon2.svg";
import icon3 from "@/public/assets/common-icons/beforeFeedback/icon3.svg";

import Image from "next/image";

import styles from "./styles.module.scss";
import { useResizeWindow } from "@/hooks/useResizeWindow";

const BeforeFeedback: FC = () => {
  const { isMobile } = useResizeWindow();
  return (
    <div className={styles.containerBeforeFeedback}>
      <Image src={group} alt="Group" className={styles.group} />
      <div className={styles.substanceRightConteiner}>
        <Image
          src={substanceRight}
          alt="substanceRight"
          className={styles.substanceRight}
        />
      </div>
      <div className={styles.substanceLeftConteiner}>
        <Image
          src={substanceLeft}
          alt="substanceLeft"
          className={styles.substanceLeft}
        />
      </div>
      {!isMobile && (
        <>
          <Image src={starWhite} alt="starWhite" className={styles.starWhite} />
          <Image src={starBlack} alt="starBlack" className={styles.starBlack} />
        </>
      )}

      <div className={styles.containerInfo}>
        <div className={styles.leftInfo}>
          <div className={styles.tx1}>
            We offer <span>unique opportunities</span> for{" "}
            <span>IT professionals</span> who are looking for not only a job,
          </div>
          <div className={styles.tx2}>
            but also the chance to <span>realize their potential</span> and be
            part of something bigger.{" "}
          </div>
        </div>
        <div className={styles.rightInfo}>
          {!isMobile && (
            <div className={styles.containerIcon}>
              <Image src={icon1} alt="icon1" className={styles.icon} />
              <Image src={icon2} alt="icon2" className={styles.icon} />
              <Image src={icon3} alt="icon3" className={styles.icon} />
            </div>
          )}

          <div className={styles.tx1}>
            With us you will find not only{" "}
            <span>
              interesting <br />
              projects,
            </span>{" "}
          </div>
          <div className={styles.tx2}>
            {" "}
            but also{" "}
            <span>
              opportunities to learn and <br /> develop
            </span>{" "}
            , participate in international <br /> conferences and workshops.
          </div>
        </div>
      </div>
    </div>
  );
};

export default BeforeFeedback;
