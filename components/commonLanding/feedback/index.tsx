"use client";
import { FC, useState } from "react";
import star from "@/public/assets/common-icons/feedback/star.svg";
import cone from "@/public/assets/common-icons/feedback/cone.png";
import glass from "@/public/assets/common-icons/feedback/glass.png";
import line from "@/public/assets/common-icons/feedback/line.svg";
import roundTube from "@/public/assets/common-icons/feedback/round-tube.png";
import ellipseDevice from "@/public/assets/common-icons/feedback/ellipse-for-device.svg";
import Image from "next/image";

import styles from "./styles.module.scss";
import SliderPhoto from "@/components/commonLanding/feedback/components/sliderPhoto";
import { Feedbacks } from "@/components/commonLanding/feedback/constants/Feedbacks";
import SliderPhotoMobile from "@/components/commonLanding/feedback/components/sliderPhotoMobile";
import { useResizeWindow } from "@/hooks/useResizeWindow";

export interface IFeedbacks {
  photo: string;
  name: string;
  profession: string;
  feedback: string;
  device: string;
}

const Feedback: FC = () => {
  const { isMobile } = useResizeWindow();
  const [selectedFeedback, setSelectedFeedback] = useState<IFeedbacks>(
    Feedbacks[0],
  );
  const [activeIndex, setActiveIndex] = useState(0);

  const handleSetActiveIndex = (index: number) => {
    setActiveIndex(index);
    setSelectedFeedback(Feedbacks[index]);
  };
  return (
    <div className={styles.containerFeedback}>
      <div className={styles.sliderPhoto}>
        {isMobile ? (
          <SliderPhotoMobile
            feedbacks={Feedbacks}
            activeIndex={activeIndex}
            setActiveIndex={handleSetActiveIndex}
          />
        ) : (
          <SliderPhoto
            feedbacks={Feedbacks}
            activeIndex={activeIndex}
            setActiveIndex={handleSetActiveIndex}
          />
        )}
      </div>
      <div className={styles.text}>
        <div className={styles.tx1}>In our long time in the market,</div>
        <div className={styles.tx2}>we have been able to help many people.</div>
        <div className={styles.tx3}>We can help you too!</div>
      </div>
      <div className={styles.info}>
        <div className={styles.subscription}>
          <div className={styles.name}>{selectedFeedback.name}</div>
          <div className={styles.profession}>{selectedFeedback.profession}</div>
        </div>
        <div className={styles.feedback}>{selectedFeedback.feedback}</div>
      </div>

      <Image src={star} alt="star" className={styles.star} />
      <Image src={cone} alt="cone" className={styles.cone} />
      <Image src={glass} alt="glass" className={styles.glass} />
      <Image src={line} alt="line" className={styles.line} />
      <Image
        src={selectedFeedback.device}
        alt="device"
        className={styles.device}
      />
      <Image src={roundTube} alt="roundTube" className={styles.roundTube} />
      <div className={styles.ellipse} />
      <div className={styles.ellipseText} />
      <Image
        src={ellipseDevice}
        alt="ellipseDevice"
        className={styles.ellipseDevice}
      />
      <div className={styles.whiteBlur} />
    </div>
  );
};

export default Feedback;
