import { IFeedbacks } from "@/components/commonLanding/feedback";
import photo2 from "@/public/assets/common-icons/feedback/photo1.png";
import photo3 from "@/public/assets/common-icons/feedback/photo2.png";
import photo4 from "@/public/assets/common-icons/feedback/photo3.png";
import photo1 from "@/public/assets/common-icons/feedback/photo4.png";
import programmerDevice from "@/public/assets/common-icons/feedback/programmer-device.png";
import designerDevice from "@/public/assets/common-icons/feedback/designer-device.png";
import seoDevice from "@/public/assets/common-icons/feedback/seo-device.png";
import testerDevice from "@/public/assets/common-icons/feedback/tester-device.png";

export const Feedbacks: IFeedbacks[] = [
  {
    photo: photo1,
    name: "Isimeli Ziegler",
    profession: "Programmer",
    feedback:
      " “Thanks to Djosu, I got a job as a developer and got\n  the opportunity to work on exciting projects”.",
    device: programmerDevice,
  },
  {
    photo: photo2,
    name: "Alieu Schickowski",
    profession: "DESIGNER",
    feedback:
      '“Djosu helped me find the design job I was looking for. I am very satisfied with the working conditions, interesting projects and the support of the team."',
    device: designerDevice,
  },
  {
    photo: photo3,
    name: "Murry Roberts",
    profession: "SEO specialist",
    feedback:
      '"Working as an SEO specialist at Djosu, I realized that this is exactly what I want to do. Professional growth, interesting projects and management support make the work fun and productive."',
    device: seoDevice,
  },
  {
    photo: photo4,
    name: "Denzel Root",
    profession: "qa Tester",
    feedback:
      '"As a quality tester, I found Djosu the perfect place to work. My work is valued here, there is an opportunity to develop and participate in interesting projects. Thank you for the opportunity to realize myself!"',
    device: testerDevice,
  },
  {
    photo: photo1,
    name: "Isimeli Ziegler",
    profession: "Programmer",
    feedback:
      " “Thanks to Djosu, I got a job as a developer and got\n  the opportunity to work on exciting projects”.",
    device: programmerDevice,
  },
  {
    photo: photo2,
    name: "Alieu Schickowski",
    profession: "DESIGNER",
    feedback:
      '“Djosu helped me find the design job I was looking for. I am very satisfied with the working conditions, interesting projects and the support of the team."',
    device: designerDevice,
  },
  {
    photo: photo3,
    name: "Murry Roberts",
    profession: "SEO specialist",
    feedback:
      '"Working as an SEO specialist at Djosu, I realized that this is exactly what I want to do. Professional growth, interesting projects and management support make the work fun and productive."',
    device: seoDevice,
  },
  {
    photo: photo4,
    name: "Denzel Root",
    profession: "qa Tester",
    feedback:
      '"As a quality tester, I found Djosu the perfect place to work. My work is valued here, there is an opportunity to develop and participate in interesting projects. Thank you for the opportunity to realize myself!"',
    device: testerDevice,
  },
];
