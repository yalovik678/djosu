import React, { FC, useState } from "react";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/swiper-bundle.css";
import styles from "./styles.module.scss";
import { Swiper as SwiperCore } from "swiper/types";
import { IFeedbacks } from "@/components/commonLanding/feedback";

interface IPhotoAddingProps {
  feedbacks: IFeedbacks[];
  activeIndex: number;
  setActiveIndex: (value: number) => void;
}

const SliderPhotoMobile: FC<IPhotoAddingProps> = ({
  feedbacks,
  activeIndex,
  setActiveIndex,
}) => {
  const handleSlideChange = (swiper: SwiperCore) => {
    setActiveIndex(swiper.realIndex);
  };

  const getRotationClass = (index: number) => {
    switch (index) {
      case activeIndex % feedbacks?.length:
        return styles.rotate15;
      case (activeIndex + 1) % feedbacks?.length:
        return styles.rotate0;
      case (activeIndex + 2) % feedbacks?.length:
        return styles.rotate24;
      case (activeIndex + 3) % feedbacks?.length:
        return styles.rotate30;
      case (activeIndex + 4) % feedbacks?.length:
        return styles.rotate40;
      default:
        return "";
    }
  };

  return (
    <div className={styles.containerMobile}>
      <Swiper
        initialSlide={activeIndex}
        onSlideChange={handleSlideChange}
        slidesPerView={3}
        watchSlidesProgress
        speed={200}
        mousewheel
        keyboard
        loop
        className={styles.mainPhotoSlider}
      >
        {feedbacks.map((feedback, index) => (
          <SwiperSlide key={index} className={styles.imagesWrapper}>
            <label
              className={`${styles.slideContainer} ${getRotationClass(index)}`}
            >
              <Image
                src={feedback.photo}
                unoptimized={false}
                alt="photo"
                className={styles.scaleAnimation}
              />
            </label>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default SliderPhotoMobile;
