import React, { FC, useRef } from "react";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import styles from "./styles.module.scss";
import { Swiper as SwiperCore } from "swiper/types";
import { IFeedbacks } from "@/components/commonLanding/feedback";

interface IPhotoAddingProps {
  feedbacks: IFeedbacks[];
  activeIndex: number;
  setActiveIndex: (value: number) => void;
}

const SliderPhoto: FC<IPhotoAddingProps> = ({
  feedbacks,
  activeIndex,
  setActiveIndex,
}) => {
  const swiperRef = useRef<SwiperCore | null>(null);

  const handleSlideChange = (swiper: SwiperCore) => {
    setActiveIndex(swiper.realIndex);
  };

  const handleSlideClick = (index: number) => {
    if (swiperRef.current) {
      if (index === 0) {
        swiperRef.current.slideToLoop(7);
        setActiveIndex(7);
      } else {
        swiperRef.current.slideToLoop(index - 1);
        setActiveIndex(index - 1);
      }
    }
  };

  const getRotationClass = (index: number) => {
    const length = feedbacks.length;
    switch ((index - activeIndex + length) % length) {
      case 0:
        return styles.rotate15;
      case 1:
        return styles.rotate0;
      case 2:
        return styles.rotate24;
      case 3:
        return styles.rotate39;
      case 4:
        return styles.rotate40;
      default:
        return "";
    }
  };

  return (
    <div className={styles.container}>
      <Swiper
        onSwiper={(swiper) => (swiperRef.current = swiper)}
        initialSlide={activeIndex}
        onSlideChange={handleSlideChange}
        slidesPerView={4}
        watchSlidesProgress
        speed={200}
        mousewheel
        keyboard
        loop
        className={styles.mainPhotoSlider}
      >
        {feedbacks?.map((feedback, index) => (
          <SwiperSlide key={index} className={styles.imagesWrapper}>
            <label
              className={`${styles.slideContainer} ${getRotationClass(index)}`}
              onClick={() => handleSlideClick(index)}
            >
              <Image
                src={feedback.photo}
                unoptimized={false}
                alt="photo"
                className={styles.scaleAnimation}
              />
            </label>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default SliderPhoto;
