import { FC, useState } from "react";
import QA from "@/components/commonLanding/checkoutQA/components/qa";
import CheckoutQaMobile from "@/components/commonLanding/checkoutQA/components/checkoutMobile";
import CheckoutDesktop from "@/components/commonLanding/checkoutQA/components/checkoutDesktop";

const CheckoutQA: FC = () => {
  const [isOpenQA, setIsOpenQA] = useState(false);
  const handleIsOpen = (value: boolean) => {
    setIsOpenQA(value);
  };
  return (
    <>
      {isOpenQA ? (
        <QA />
      ) : (
        <>
          <CheckoutDesktop setIsOpenQA={handleIsOpen} />
          <CheckoutQaMobile setIsOpenQA={handleIsOpen} />
        </>
      )}
    </>
  );
};

export default CheckoutQA;
