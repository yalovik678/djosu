import { IQAData } from "@/components/designersLanding/components/qa";
import { qaDataDesigners } from "@/components/designersLanding/components/qa/constants/qaDataDesigners";
import { qaDataDevelopers } from "@/components/developersLanding/components/qa/constants/qaDataDevelopers";
import { qaDataTesters } from "@/components/testersLanding/components/qa/constants/qaDataTesters";
import { qaDataStrategists } from "@/components/strategistslanding/components/qa/constants/qaDataStrategists";
import { qaDataSeo } from "@/components/seoLanding/componens/qa/constants/qaDataSeo";
import arrowDesigners from "@/public/assets/designers-icons/qa/arrow.svg";
import arrowDevelopers from "@/public/assets/developers-icons/qa/arrow.svg";
import arrowTester from "@/public/assets/testers-icons/qa/arrow.svg";
import arrowStrategist from "@/public/assets/strategists-icons/qa/arrow.svg";
import arrowSEO from "@/public/assets/seo-icons/qa/arrow.svg";

import styles from "./styles.module.scss";

export interface IButtonsQA {
  id: number;
  name: string;
  data: IQAData[];
  stylesQA?: string;
  stylesButton?: string;
  colorQuestion?: string;
  arrow: string;
}

export const buttonsQA: IButtonsQA[] = [
  {
    id: 0,
    name: "Designers",
    data: qaDataDesigners,
    stylesQA: styles.QADesigners,
    stylesButton: styles.btnDesigners,
    colorQuestion: styles.questionDesigners,
    arrow: arrowDesigners,
  },
  {
    id: 1,
    name: "Developers",
    data: qaDataDevelopers,
    stylesQA: styles.QADevelopers,
    stylesButton: styles.btnDevelopers,
    colorQuestion: styles.questionDevelopers,
    arrow: arrowDevelopers,
  },
  {
    id: 2,
    name: "QA Tester",
    data: qaDataTesters,
    stylesQA: styles.QATesters,
    stylesButton: styles.QATesters,
    colorQuestion: styles.questionTesters,
    arrow: arrowTester,
  },
  {
    id: 3,
    name: "Social Media Strategist",
    data: qaDataStrategists,
    stylesQA: styles.QAStrategists,
    stylesButton: styles.btnStrategists,
    colorQuestion: styles.questionStrategists,
    arrow: arrowStrategist,
  },
  {
    id: 4,
    name: "SEO specialists",
    data: qaDataSeo,
    stylesQA: styles.QASEO,
    stylesButton: styles.btnSEO,
    colorQuestion: styles.questionSEO,
    arrow: arrowSEO,
  },
];
