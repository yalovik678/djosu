import { FC, useState } from "react";
import liquida from "@/public/assets/designers-icons/qa/mujer-liquida.png";
import substanceLeft from "@/public/assets/designers-icons/qa/substance-left.png";
import glass13 from "@/public/assets/icons/glass-13.png";
import Image from "next/image";
import cn from "classnames";

import styles from "./styles.module.scss";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import { buttonsQA } from "@/components/commonLanding/checkoutQA/constants/buttonsQA";
import SliderButtons from "@/components/commonLanding/checkoutQA/components/qa/componens/sliderButtons";
import AfterQAMobile from "@/components/commonLanding/checkoutQA/components/afterQAMobile";

export interface IQAData {
  id: number;
  question: string;
  answer: JSX.Element;
}

const QA: FC = () => {
  const { isMobile } = useResizeWindow();
  const [selectedQA, setSelectedQA] = useState(0);
  const [selectedButtonId, setSelectedButtonId] = useState(1);

  const handleSetActiveIndex = (index: number) => {
    setSelectedButtonId(index);
  };

  const handleClickButton = (id: number) => {
    setSelectedButtonId(id);
  };

  const handleClick = (id: number) => {
    if (selectedQA === id) {
      setSelectedQA(0);
    } else {
      setSelectedQA(id);
    }
  };

  return (
    <div className={styles.containerQA}>
      {isMobile ? (
        <>
          <div className={styles.text1}>
            Djosu invites talented IT specialists to join its team!
          </div>
        </>
      ) : (
        <>
          <div className={styles.text1}>
            Djosu invites talented IT specialists to join its team!
          </div>
          <div className={styles.text2}>
            With us you will find excellent working conditions,
          </div>
          <div className={styles.text3}>
            interesting projects and opportunity for professional growth.
          </div>
        </>
      )}
      {isMobile && (
        <SliderButtons
          buttonsQA={buttonsQA}
          activeIndex={selectedButtonId}
          setActiveIndex={handleSetActiveIndex}
        />
      )}

      <div className={cn(styles.QAs, buttonsQA[selectedButtonId].stylesQA)}>
        {!isMobile && (
          <div className={styles.containerButtons}>
            <div className={styles.row}>
              {buttonsQA.slice(0, 2).map((button) => (
                <button
                  key={button.id}
                  onClick={() => handleClickButton(button.id)}
                  className={cn(
                    button.id === selectedButtonId &&
                      buttonsQA[selectedButtonId].stylesButton,
                  )}
                >
                  {button.name}
                </button>
              ))}
            </div>
            <div className={styles.row}>
              {buttonsQA.slice(2, 5).map((button) => (
                <button
                  key={button.id}
                  onClick={() => handleClickButton(button.id)}
                  className={cn(
                    button.id === selectedButtonId &&
                      buttonsQA[selectedButtonId].stylesButton,
                  )}
                >
                  {button.name}
                </button>
              ))}
            </div>
          </div>
        )}

        {buttonsQA[selectedButtonId].data.map((qa) => (
          <div
            key={qa.id}
            className={styles.QA}
            onClick={() => handleClick(qa.id)}
          >
            <div
              className={cn(
                styles.question,
                buttonsQA[selectedButtonId].colorQuestion,
              )}
            >
              <div className={styles.text}>{qa.question}</div>
              <Image
                src={buttonsQA[selectedButtonId].arrow}
                alt="arrow"
                className={cn(styles.arrow, {
                  [styles.activeArrow]: selectedQA === qa.id,
                })}
              />
            </div>
            {selectedQA === qa.id && qa.answer}
          </div>
        ))}
        <div
          className={cn(
            styles.bottomText,
            buttonsQA[selectedButtonId].colorQuestion,
          )}
        >
          Join us and become part of a successful company where your
          contribution will be appreciated!
        </div>
      </div>

      <div className={styles.glass13}>
        <Image src={glass13} alt="glass13" />
      </div>

      <Image src={liquida} alt="liquida" className={styles.liquida} />
      <Image
        src={substanceLeft}
        alt="substanceLeft"
        className={styles.substanceLeft}
      />
      <AfterQAMobile />
    </div>
  );
};

export default QA;
