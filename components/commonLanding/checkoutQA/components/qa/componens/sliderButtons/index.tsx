import React, { FC, useRef } from "react";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import styles from "./styles.module.scss";
import { Swiper as SwiperCore } from "swiper/types";
import { IButtonsQA } from "@/components/commonLanding/checkoutQA/constants/buttonsQA";
import cn from "classnames";

interface ISliderButtonsProps {
  buttonsQA: IButtonsQA[];
  activeIndex: number;
  setActiveIndex: (value: number) => void;
}

const SliderButtons: FC<ISliderButtonsProps> = ({
  buttonsQA,
  activeIndex,
  setActiveIndex,
}) => {
  const swiperRef = useRef<SwiperCore | null>(null);

  const handleSlideChange = (swiper: SwiperCore) => {
    setActiveIndex(swiper.realIndex);
  };

  const handleSlideClick = (index: number) => {
    console.log(index);
    if (swiperRef.current) {
      swiperRef.current.slideToLoop(index);
      setActiveIndex(index);
    }
  };

  return (
    <div
      className={cn(
        styles.container,
        activeIndex === 2 && styles.containerTester,
        activeIndex === 3 && styles.containerMedia,
        activeIndex === 4 && styles.containerSEO,
      )}
    >
      <Swiper
        onSwiper={(swiper) => (swiperRef.current = swiper)}
        initialSlide={activeIndex}
        onSlideChange={handleSlideChange}
        slidesPerView={3}
        watchSlidesProgress
        speed={200}
        mousewheel
        keyboard
        loop
        className={styles.mainPhotoSlider}
      >
        {buttonsQA?.map((button, index) => (
          <SwiperSlide key={button.id} className={styles.imagesWrapper}>
            <button
              className={cn(index === activeIndex && button.stylesButton)}
              onClick={() => handleSlideClick(index)}
            >
              {button.name}
            </button>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default SliderButtons;
