import { FC } from "react";
import substanceLeft from "@/public/assets/common-icons/beforeQA/substance-left.png";
import liquidaRight from "@/public/assets/common-icons/beforeQA/mujer-liquida-right.png";
import liquidaLeft from "@/public/assets/common-icons/qa/mujer-liquida.png";
import baseImage from "@/public/assets/common-icons/beforeQA/base-image.png";
import box from "@/public/assets/common-icons/qa/box.png";
import spiral from "@/public/assets/common-icons/qa/spiral.png";
import substance from "@/public/assets/common-icons/qa/substance.png";
import arrow from "@/public/assets/common-icons/qa/arrow.png";

import Image from "next/image";

import styles from "./styles.module.scss";

interface ICheckoutMobile {
  setIsOpenQA: (value: boolean) => void;
}

const AfterQAMobile: FC<ICheckoutMobile> = ({ setIsOpenQA }) => {
  return (
    <div className={styles.containerAfterQAMobile}>
      <div className={styles.text}>
        <span>Together we can change the world</span> for the better, be part of
        <span>Djosu&apos;s success story!</span>
      </div>
      <Image src={baseImage} alt="baseImage" className={styles.baseImage} />
      <Image src={box} alt="box" className={styles.box} />
      <Image src={spiral} alt="spiral" className={styles.spiral} />
      <Image src={substance} alt="substance" className={styles.substance} />
    </div>
  );
};

export default AfterQAMobile;
