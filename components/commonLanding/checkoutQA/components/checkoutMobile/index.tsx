import { FC } from "react";
import substanceLeft from "@/public/assets/common-icons/beforeQA/substance-left.png";
import liquidaRight from "@/public/assets/common-icons/beforeQA/mujer-liquida-right.png";
import liquidaLeft from "@/public/assets/common-icons/qa/mujer-liquida.png";
import baseImage from "@/public/assets/common-icons/beforeQA/base-image.png";
import box from "@/public/assets/common-icons/qa/box.png";
import spiral from "@/public/assets/common-icons/qa/spiral.png";
import substance from "@/public/assets/common-icons/qa/substance.png";
import arrow from "@/public/assets/common-icons/qa/arrow.png";

import Image from "next/image";

import styles from "./styles.module.scss";

interface ICheckoutMobile {
  setIsOpenQA: (value: boolean) => void;
}

const CheckoutQaMobile: FC<ICheckoutMobile> = ({ setIsOpenQA }) => {
  return (
    <div className={styles.containerBlock78}>
      <Image
        src={substanceLeft}
        alt="substanceLeft"
        className={styles.substanceLeft}
      />{" "}
      <Image
        src={liquidaRight}
        alt="liquidaRight"
        className={styles.liquidaRight}
      />
      <Image
        src={liquidaLeft}
        alt="liquidaLeft"
        className={styles.liquidaLeft}
      />
      <div className={styles.tex1}>
        For many years now, <span>Djosu</span> has been considered one of{" "}
        <span>the best</span> in{" "}
        <span>the information technology industry.</span>
      </div>
      <div className={styles.tex2}>
        Join us and become part of a successful company where your contribution
        will be appreciated!
      </div>
      <div className={styles.toQA} onClick={() => setIsOpenQA(true)}>
        <div className={styles.tx1}>Want to know more?</div>
        <div className={styles.tx2}>Check out the Q&A!</div>
        <Image src={arrow} alt="arrow" className={styles.arrow} />
      </div>
      <div className={styles.text}>
        <span>Together we can change the world</span> for the better, be part of{" "}
        <span>Djosu&apos;s success story!</span>
      </div>
      <Image src={baseImage} alt="baseImage" className={styles.baseImage} />
      <Image src={box} alt="box" className={styles.box} />
      <Image src={spiral} alt="spiral" className={styles.spiral} />
      <Image src={substance} alt="substance" className={styles.substance} />
    </div>
  );
};

export default CheckoutQaMobile;
