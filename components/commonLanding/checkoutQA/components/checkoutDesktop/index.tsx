import { FC, useState } from "react";
import baseImage from "@/public/assets/common-icons/qa/base-image.png";
import box from "@/public/assets/common-icons/qa/box.png";
import spiral from "@/public/assets/common-icons/qa/spiral.png";
import arrow from "@/public/assets/common-icons/qa/arrow.png";
import substance from "@/public/assets/common-icons/qa/substance.png";
import liquida from "@/public/assets/common-icons/qa/mujer-liquida.png";

import Image from "next/image";

import styles from "./styles.module.scss";

interface ICheckoutDesktop {
  setIsOpenQA: (value: boolean) => void;
}

const CheckoutDesktop: FC<ICheckoutDesktop> = ({ setIsOpenQA }) => {
  return (
    <div className={styles.containerBlock8}>
      <div className={styles.text}>
        <span>Together we can change the world</span> for the better, be part of{" "}
        <span>Djosu&apos;s success story!</span>
      </div>
      <Image src={baseImage} alt="baseImage" className={styles.baseImage} />
      <Image src={box} alt="box" className={styles.box} />
      <Image src={spiral} alt="spiral" className={styles.spiral} />
      <div className={styles.toQA} onClick={() => setIsOpenQA(true)}>
        <div className={styles.tx1}>Want to know more?</div>
        <div className={styles.tx2}>Check out the Q&A!</div>
        <Image src={arrow} alt="arrow" className={styles.arrow} />
      </div>
      <Image src={substance} alt="substance" className={styles.substance} />
      <Image src={liquida} alt="liquida" className={styles.liquida} />
    </div>
  );
};

export default CheckoutDesktop;
