"use client";

import AboutUs from "@/components/commonLanding/aboutUs";
import { FC, RefObject, useEffect, useRef, useState } from "react";
import Header from "@/components/designersLanding/components/header";
import Vacancies from "@/components/designersLanding/components/vacancies";
import Benefits1 from "./components/benefits1";
import Benefits2 from "./components/benefits2";
import Feedback from "./components/feedback";
import QA from "./components/qa";
import qa from "@/components/designersLanding/components/qa";

export const DesignersLanding: FC = () => {
  const headerRef = useRef<HTMLDivElement>(null);
  const feedbackRef = useRef<HTMLDivElement>(null);
  const QARef = useRef<HTMLDivElement>(null);
  const vacanciesRef = useRef<HTMLDivElement>(null);
  const benefitsRef = useRef<HTMLDivElement>(null);
  const [selectedRef, setSelectedRef] =
    useState<RefObject<HTMLDivElement>>(headerRef);

  useEffect(() => {
    if (selectedRef) {
      selectedRef.current?.scrollIntoView({ behavior: "smooth" });
    }
  }, [selectedRef]);

  const handleSetRef = (id: number) => {
    switch (id) {
      case 0:
        setSelectedRef(vacanciesRef);
        break;
      case 1:
        setSelectedRef(benefitsRef);
        break;
      case 2:
        setSelectedRef(feedbackRef);
        break;
      case 3:
        setSelectedRef(QARef);
        break;
      default:
        console.log("here");
        break;
    }
  };

  return (
    <div>
      {/*<div ref={headerRef}>*/}
      <Header setSelectedRef={handleSetRef} />
      {/*</div>*/}
      <div ref={vacanciesRef}>
        <Vacancies />
      </div>

      <div ref={benefitsRef}>
        <Benefits1 />
      </div>

      <Benefits2 />
      <div ref={feedbackRef}>
        <Feedback />
      </div>
      <div ref={QARef}>
        <QA />
      </div>

      <AboutUs />
    </div>
  );
};
