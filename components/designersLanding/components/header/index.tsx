"use client";
import { FC } from "react";

import eLetter from "@/public/assets/designers-icons/header/e.svg";
import uLetter from "@/public/assets/designers-icons/header/u.svg";
import rectangles from "@/public/assets/designers-icons/header/rectangles.svg";
import rectanglesMobile from "@/public/assets/designers-icons/header/rectangles-mobile.svg";
import cone from "@/public/assets/designers-icons/header/cone.png";
import cylinderLeft from "@/public/assets/designers-icons/header/cylinder-left.png";
import cylinderRight from "@/public/assets/designers-icons/header/cylinder-right.png";

import styles from "./styles.module.scss";
import Image from "next/image";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import Menu from "@/components/lips/menu";

interface IHeaderProps {
  setSelectedRef: (id: number) => void;
}

const Header: FC<IHeaderProps> = ({ setSelectedRef }) => {
  const { isMobile } = useResizeWindow();

  const handleFeedbackClick = () => {
    setSelectedRef(0);
  };

  const handleAboutUsClick = () => {
    setSelectedRef(1);
  };

  const handleVacanciesClick = () => {
    setSelectedRef(2);
  };

  const handleBenefitsClick = () => {
    setSelectedRef(3);
  };

  return (
    <div className={styles.wrapper}>
      <Menu
        setSelectedRef={setSelectedRef}
        className={styles.menu}
        classNameBurgerMenu={styles.burgerMenu}
      />

      <div className={styles.title}>
        <div className={styles.row1}>
          <span>D</span>
          <Image src={eLetter} alt="eLetter" className={styles.eLetter} />
          <span className={styles.sp1}>sign</span>
        </div>
        <div className={styles.row2}>
          <span className={styles.sp2}>yo</span>
          <Image src={uLetter} alt="uLetter1" className={styles.uLetter1} />
          <span className={styles.sp1}>r</span>
        </div>
        <div className={styles.row3}>
          <span>s</span>
          <Image src={uLetter} alt="uLetter2" className={styles.uLetter2} />
          <span className={styles.sp1}>ccess</span>
        </div>
      </div>
      {isMobile ? (
        <Image
          src={rectanglesMobile}
          alt="rectanglesMobile"
          className={styles.rectangles}
        />
      ) : (
        <Image
          src={rectangles}
          alt="rectangles"
          className={styles.rectangles}
        />
      )}

      <div className={styles.extraMenu}>
        <div className={styles.twoButtons1}>
          <button className={styles.btn1} onClick={handleFeedbackClick}>
            Vacancies
          </button>
          <button className={styles.btn2} onClick={handleAboutUsClick}>
            Benefits
          </button>
        </div>
        <div className={styles.twoButtons2}>
          <button className={styles.btn1} onClick={handleVacanciesClick}>
            Feedback
          </button>
          <button className={styles.btmDM} onClick={handleBenefitsClick}>
            Q&A
          </button>
        </div>
      </div>
      <Image src={cone} alt="cone" className={styles.cone} />
      <Image
        src={cylinderLeft}
        alt="cylinderLeft"
        className={styles.cylinderLeft}
      />
      <Image
        src={cylinderRight}
        alt="cylinderRight"
        className={styles.cylinderRight}
      />
      <div className={styles.text1}>Do you want to be a </div>
      <div className={styles.text2}>designer on our team? </div>
    </div>
  );
};
export default Header;
