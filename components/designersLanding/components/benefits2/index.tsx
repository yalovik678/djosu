import { FC } from "react";
import roundCube from "@/public/assets/designers-icons/benefits2/roundCube.png";
import superToroidLeft from "@/public/assets/designers-icons/benefits2/superToroid-left.png";
import superToroidRight from "@/public/assets/designers-icons/benefits2/superToroid-right.png";
import become from "@/public/assets/designers-icons/benefits2/become.png";
import pill from "@/public/assets/designers-icons/benefits2/pill.png";
import substance from "@/public/assets/designers-icons/benefits2/substance.png";
import blurRight from "@/public/assets/designers-icons/benefits2/blur-right.svg";
import djosuText3 from "@/public/assets/designers-icons/benefits2/djosuText3.png";
import djosuText3Mobile from "@/public/assets/designers-icons/benefits2/djosu-3-mobile.png";
import blurLeft from "@/public/assets/designers-icons/benefits2/blur-left.svg";
import Image from "next/image";

import styles from "./styles.module.scss";
import CustomButton from "@/components/lips/customButton";

const Benefits2: FC = () => {
  return (
    <div className={styles.containerBenefits2}>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <Image src={roundCube} alt="roundCube" className={styles.roundCube} />
      <Image
        src={superToroidLeft}
        alt="superToroidLeft"
        className={styles.superToroidLeft}
      />{" "}
      <Image src={become} alt="become" className={styles.become} />
      <Image src={pill} alt="pill" className={styles.pill} />
      <Image
        src={superToroidRight}
        alt="superToroidRight"
        className={styles.superToroidRight}
      />{" "}
      <Image src={substance} alt="substance" className={styles.substance} />
      <Image src={blurRight} alt="blurRight" className={styles.blurRight} />
      <Image src={djosuText3} alt="djosuText3" className={styles.djosuText3} />
      <Image
        src={djosuText3Mobile}
        alt="djosuText3Mobile"
        className={styles.djosuText3Mobile}
      />
      <Image src={blurLeft} alt="blurLeft" className={styles.blurLeft} />
    </div>
  );
};

export default Benefits2;
