import { FC, useState } from "react";
import arrow from "@/public/assets/designers-icons/qa/arrow.svg";
import glass13 from "@/public/assets/icons/glass-13.png";
import liquida3 from "@/public/assets/icons/mujer-liquida-3.png";
import liquida4 from "@/public/assets/icons/mujer-liquida-4.png";
import substanceLeft from "@/public/assets/designers-icons/qa/substance-left.png";
import Image from "next/image";
import cn from "classnames";

import styles from "./styles.module.scss";
import { qaDataDesigners } from "@/components/designersLanding/components/qa/constants/qaDataDesigners";
import { useResizeWindow } from "@/hooks/useResizeWindow";
import BlurSvgComponent from "@/components/designersLanding/components/qa/components/blurSvgComponent";

export interface IQAData {
  id: number;
  question: string;
  answer: JSX.Element;
}

const QA: FC = () => {
  const { isMobile } = useResizeWindow();
  const [selectedQA, setSelectedQA] = useState(0);

  const handleClick = (id: number) => {
    if (selectedQA === id) {
      setSelectedQA(0);
    } else {
      setSelectedQA(id);
    }
  };

  return (
    <div className={styles.containerQA}>
      {isMobile ? (
        <>
          <div className={styles.text1}>
            For many years now, <span>Djosu</span> has been considered one of{" "}
            <span>the best in the information technology industry.</span>
          </div>
          <div className={styles.text2}>
            Join us and become part of a successful company where your
            contribution will be appreciated!
          </div>
        </>
      ) : (
        <>
          <div className={styles.text1}>
            Djosu invites talented IT specialists to join its team!
          </div>
          <div className={styles.text2}>
            With us you will find excellent working conditions,
          </div>
          <div className={styles.text3}>
            interesting projects and opportunity for professional growth.
          </div>
        </>
      )}
      {isMobile && (
        <div className={styles.toQA}>
          <div className={styles.tx1}>Want to know more?</div>
          <div className={styles.tx2}>Check out the Q&A!</div>
          <Image src={arrow} alt="arrow" className={styles.arrow} />
        </div>
      )}

      <div className={styles.QAs}>
        {qaDataDesigners.map((qa) => (
          <div key={qa.id} className={styles.QA}>
            <div className={styles.question} onClick={() => handleClick(qa.id)}>
              <div className={styles.text}>{qa.question}</div>
              <Image
                src={arrow}
                alt="arrow"
                className={cn(styles.arrow, {
                  [styles.activeArrow]: selectedQA === qa.id,
                })}
              />
            </div>
            {selectedQA === qa.id && qa.answer}
          </div>
        ))}
        <div className={styles.bottomText}>
          Join us and become part of a successful company where your
          contribution will be appreciated!
        </div>
      </div>
      <div className={styles.liquida3}>
        <Image src={liquida3} alt="liquida3" />
      </div>

      <Image src={liquida4} alt="liquida4" className={styles.liquida4} />
      <Image
        src={substanceLeft}
        alt="substanceLeft"
        className={styles.substanceLeft}
      />
      <Image src={glass13} alt="glass13" className={styles.glass13} />
      <BlurSvgComponent />
    </div>
  );
};

export default QA;
