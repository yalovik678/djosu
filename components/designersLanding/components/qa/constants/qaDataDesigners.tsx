import { IQAData } from "@/components/designersLanding/components/qa";
import styles from "./styles.module.scss";

export const qaDataDesigners: IQAData[] = [
  {
    id: 1,
    question:
      "What projects are expected of new designers in the company and what are the requirements for them?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          Djosu aims to continue championing the future of design by nurturing
          emerging talent and fostering creativity. Whether you’re a recent
          graduate or an experienced designer, Djosu welcomes those who share
          their vision of pushing design boundaries.
        </div>
        <div className={styles.text}>Here are some factors to consider:</div>
        <ul>
          <li>
            <span>Passion and Creativity:</span>
          </li>
          <div className={styles.notLeft}>
            Djosu seeks designers who are passionate about their craft and
            demonstrate creativity.
          </div>
          <li>
            <span>Adaptability:</span>
          </li>
          <div className={styles.notLeft}>
            New designers should be adaptable, open to learning, and willing to
            explore new ideas.
          </div>
          <li>
            <span>Collaboration:</span>
          </li>
          <div className={styles.notLeft}>
            A collaborative mindset is essential, as Djosu values teamwork and
            cross-disciplinary interactions.
          </div>
          <li>
            <span>Portfolio:</span>
          </li>
          <div className={styles.notLeft}>
            A strong portfolio showcasing design projects, concepts, and
            problem-solving abilities is crucial.
          </div>
          <li>
            <span>Communication Skills:</span>
          </li>
          <div className={styles.notLeft}>
            Effective communication—both visual and verbal—is highly valued.
          </div>
          <li>
            <span>Resilience:</span>
          </li>
          <div className={styles.notLeft}>
            The design industry can be challenging; resilience and a growth
            mindset are valuable traits.
          </div>
        </ul>
      </div>
    ),
  },
  {
    id: 2,
    question:
      "What percentage of designers' time will be spent on remote work and is business travel possible?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          In adherence to the evolving work paradigm, Djosu extends the
          opportunity for designers to engage in remote work, with a current
          trend indicating 12.7% of employees operating full-time from remote
          locations and 28.2% participating in a hybrid model.
        </div>
        <div className={styles.text}>
          <span>
            This approach is reflective of our commitment to flexibility and
            innovation.
          </span>
        </div>
        <div className={styles.text}>
          Regarding business travel, it is anticipated to resume, aligning with
          global trends and the resurgence of “bleisure” travel, which combines
          business engagements with leisure activities. This development is
          indicative of the dynamic and interconnected nature of our operations.
        </div>
      </div>
    ),
  },
  {
    id: 3,
    question:
      "What programs and tools do designers use in the company and which ones will be provided to employees?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          At Djosu, we are at the forefront of design innovation, and our
          toolkit reflects this commitment.
        </div>
        <div className={styles.text}>
          <span>
            Our designers are equipped with industry-leading software such as
            Adobe Illustrator for vector graphics, Photoshop for image editing,
            and InDesign for layout tasks.
          </span>
        </div>
        <div className={styles.text}>
          These applications are complemented by Figma and Sketch, which enhance
          collaborative design and UX/UI development.
        </div>
        <div className={styles.text}>
          Further enhancing our digital environment, Figma and Sketch are
          provided to foster collaboration and refine user experience design.
          These tools are integral to our ethos of innovation and are
          instrumental in the execution of our visionary projects.
        </div>
      </div>
    ),
  },
  {
    id: 4,
    question:
      "What are the career prospects for designers at Djosu and how are their contributions to the success of projects valued?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          At Djosu, we recognize that designers are the architects of innovation
          and the cornerstone of our success. Our commitment to fostering a
          culture of creativity means we value each designer’s unique
          perspective and contributions. As a designer at Djosu, you will embark
          on a journey where your skills will not only be nurtured but
          celebrated.
        </div>
        <ul>
          <li>
            <span>Career Prospects:</span>
          </li>
          <div className={styles.notLeft}>
            Designers at Djosu can look forward to a dynamic career trajectory.
            With roles ranging from graphic design to product design, there is a
            spectrum of opportunities to explore. We ensure that our designers
            are equipped with the latest tools and knowledge through continuous
            learning initiatives, keeping them at the forefront of the industry.
          </div>
          <li>
            <span>Valuing Contributions:</span>
          </li>
          <div className={styles.notLeft}>
            We understand that the success of our projects hinges on the
            ingenuity and dedication of our designers. That’s why we have a
            comprehensive system to recognize and reward contributions. From
            spotlighting achievements in company-wide meetings to offering
            tangible rewards like bonuses and promotions, we ensure that your
            hard work and success do not go unnoticed.
          </div>
          <li>
            <span>Innovation and Success:</span>
          </li>
          <div className={styles.notLeft}>
            At Djosu, designers are integral to our project lifecycle. Your
            ideas will fuel groundbreaking projects, and your designs will shape
            the user experience that defines our products. We believe in giving
            our designers the freedom to experiment and innovate, which in turn
            drives the success of our projects.
          </div>
        </ul>
      </div>
    ),
  },
  {
    id: 5,
    question:
      "What kind of training and professional development can designers receive in the company, are there mentorship programs or internal trainings?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          At Djosu, we are passionate about the growth and development of our
          designers. We believe that continuous learning is the key to personal
          and professional success, which is why we offer a robust suite of
          training and professional development opportunities tailored to the
          unique needs of our design team.
        </div>
        <ul>
          <li>
            <span>Your Path at Djosu:</span>
          </li>
          <div className={styles.notLeft}>
            Joining Djosu means embarking on a journey of endless learning and
            growth. Whether through formal training, mentorship, or peer-led
            sessions, we ensure that our designers have the resources they need
            to excel. If you’re looking for a place where your career can
            flourish and where your contributions are valued, Djosu is waiting
            for you. Let’s craft beautiful designs and a brighter future
            together.
          </div>
          <li>
            <span>Internal Trainings:</span>
          </li>
          <div className={styles.notLeft}>
            Djosu is committed to leveraging the wealth of knowledge within our
            own walls. Our internal training initiatives are led by our most
            experienced designers and cover topics such as best practices,
            design innovation, and project management. These sessions not only
            enhance skills but also strengthen the bonds within our design
            community.
          </div>
          <li>
            <span>Mentorship Programs:</span>
          </li>
          <div className={styles.notLeft}>
            We understand the value of guidance and support in a designer’s
            career. That’s why we have established mentorship programs that pair
            less experienced designers with seasoned professionals. These
            relationships foster a culture of knowledge-sharing and
            collaboration, allowing our designers to gain insights and advice
            from experts within their field.
          </div>
          <li>
            <span>Training and Professional Development:</span>
          </li>
          <div className={styles.notLeft}>
            Our designers benefit from a comprehensive Continuing Professional
            Development (CPD) program that ensures they stay ahead of the curve
            in the ever-evolving world of design. This includes access to
            cutting-edge courses, workshops, and seminars that cover a wide
            range of topics from technical skills to creative thinking.
          </div>
        </ul>
      </div>
    ),
  },
  {
    id: 6,
    question:
      "What corporate events and activities are organized to maintain team spirit and motivate employees?",
    answer: (
      <div className={styles.answer}>
        <div className={styles.text}>
          At Djosu, we believe that a vibrant team spirit is the lifeblood of
          our company’s success. That’s why we’ve crafted a calendar of
          corporate events and activities designed to not only maintain but
          elevate the morale and motivation of our employees.
        </div>
        <ul>
          <li>
            <span>Company Fun Days:</span>
          </li>
          <div className={styles.notLeft}>
            Our annual Company Fun Days are legendary, offering a delightful
            escape from the office into a day brimming with activities and
            laughter. Picture a quintessential English garden party with
            traditional games like rounders, croquet, and giant Jenga,
            culminating in a friendly tug-of-war showdown. It’s a day where
            camaraderie blossoms, and memories are made.
          </div>
          <li>
            <span>Family Fun Days:</span>
          </li>
          <div className={styles.notLeft}>
            We extend our appreciation to the families of our employees with our
            Family Fun Days. These events are a testament to our commitment to
            work-life balance, providing a fun and inclusive environment where
            families can join in the festivities. It’s a perfect blend of
            adventure and relaxation, ensuring that everyone, from the youngest
            to the oldest, has a fantastic time.
          </div>
          <li>
            <span>Creative Workshops:</span>
          </li>
          <div className={styles.notLeft}>
            To spark innovation and creativity, we host various workshops
            throughout the year. These sessions are led by industry experts and
            cover a wide range of topics, from design thinking to digital
            artistry. They’re a chance for our team to learn, grow, and be
            inspired.
          </div>
          <li>
            <span>Team Building Activities:</span>
          </li>
          <div className={styles.notLeft}>
            Teamwork is at the core of what we do, and our team building
            activities are designed to strengthen this foundation. From escape
            rooms to treasure hunts, we provide opportunities for our employees
            to collaborate, solve problems, and have fun in the process.
          </div>
          <li>
            <span>Recognition Ceremonies:</span>
          </li>
          <div className={styles.notLeft}>
            We celebrate the achievements of our team with recognition
            ceremonies that honor the hard work and dedication of our employees.
            These events are a formal tip of the hat to those who go above and
            beyond, reinforcing the value we place on excellence.
          </div>
          <div className={styles.notLeft}>
            If you’re seeking a career where your well-being is a priority,
            where your talents are nurtured, and where every day offers a chance
            to connect and celebrate with your colleagues, look no further than
            Djosu.
          </div>
          <div className={styles.notLeft}>
            <span>
              Here, you’re not just an employee; you’re part of a community that
              thrives on enthusiasm and collective success.
            </span>
          </div>
        </ul>
      </div>
    ),
  },
];
