import React from "react";
import styles from "./styles.module.scss";

const BlurSvgComponent = () => {
  return (
    <div className={styles.svgContainer}>
      <svg
        width="1098"
        height="1701"
        viewBox="0 0 1098 1701"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g filter="url(#filter0_f_22_2265)">
          <path
            d="M1460.43 1190.4C1714.79 1086.11 1774.27 833.823 1593.27 626.896C1412.27 419.969 1059.34 336.765 804.969 441.055L633.871 511.204C379.503 615.494 320.025 867.785 501.025 1074.71C682.024 1281.64 1034.96 1364.84 1289.33 1260.55L1460.43 1190.4Z"
            fill="url(#paint0_radial_22_2265)"
          />
        </g>
        <defs>
          <filter
            id="filter0_f_22_2265"
            x="0.858887"
            y="0.64624"
            width="2092.58"
            height="1700.32"
            filterUnits="userSpaceOnUse"
            colorInterpolationFilters="sRGB"
          >
            <feFlood floodOpacity="0" result="BackgroundImageFix" />
            <feBlend
              mode="normal"
              in="SourceGraphic"
              in2="BackgroundImageFix"
              result="shape"
            />
            <feGaussianBlur
              stdDeviation="197.711"
              result="effect1_foregroundBlur_22_2265"
            />
          </filter>
          <radialGradient
            id="paint0_radial_22_2265"
            cx="0"
            cy="0"
            r="1"
            gradientUnits="userSpaceOnUse"
            gradientTransform="translate(976.013 930.937) rotate(-155.038) scale(606.724 500.275)"
          >
            <stop stopColor="#5BF5FF" />
            <stop offset="1" stopColor="#7A97FF" stopOpacity="0.31" />
          </radialGradient>
        </defs>
      </svg>
    </div>
  );
};

export default BlurSvgComponent;
