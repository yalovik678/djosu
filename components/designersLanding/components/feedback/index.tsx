import { FC } from "react";
import iconGil from "@/public/assets/designers-icons/feedback/icon-Gil.png";
import iconRirchie from "@/public/assets/designers-icons/feedback/icon-Rirchie.png";
import iconSweyn from "@/public/assets/designers-icons/feedback/icon-Sweyn.png";
import roundCube from "@/public/assets/designers-icons/feedback/roundCube.png";
import pill from "@/public/assets/designers-icons/feedback/pill.png";
import substance from "@/public/assets/designers-icons/feedback/substance.png";

import Image from "next/image";

import styles from "./styles.module.scss";
import cn from "classnames";
import CustomButton from "@/components/lips/customButton";

const Feedback: FC = () => {
  return (
    <div className={styles.containerFeedback}>
      <div className={styles.title}>Real reviews!</div>
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />

      <div className={styles.feedback1}>
        <div className={styles.text}>
          Employees of the company are always attentive to the needs of the
          customer and try to offer the best option for cooperation.
        </div>
        <div className={styles.name}>
          <Image src={iconGil} alt="iconGil" className={styles.iconFeedback} />
          <div>Gil Illingsworth</div>
        </div>
      </div>

      <div className={cn(styles.feedback1, styles.feedback2)}>
        <div className={styles.text}>
          <div className={styles.tx1}>
            One of the advantages of the company is the possibility of
            cooperation with experienced designers.
          </div>
          <div className={styles.rect} />
          <div className={styles.tx2}>
            This allows us to find optimal solutions for the most complex tasks
            and create high-quality and effective projects.
          </div>
        </div>
        <div className={styles.name}>
          <Image
            src={iconSweyn}
            alt="iconSweyn"
            className={styles.iconFeedback}
          />
          <div>Sweyn Yates</div>
        </div>
      </div>

      <div className={cn(styles.feedback1, styles.feedback3)}>
        <div className={styles.text}>
          Djosu,
          <br />
          is a great choice for designers and creative professionals. <br />
          <span>
            It employs highly qualified specialists who are always ready to help
            and support.
          </span>
        </div>
        <div className={styles.name}>
          <Image
            src={iconRirchie}
            alt="iconRirchie"
            className={styles.iconFeedback}
          />
          <div>Ritchie Bauer</div>
        </div>
      </div>
      <div className={styles.rectShadow} />
      <Image src={roundCube} alt="roundCube" className={styles.roundCube} />
      <Image src={pill} alt="pill" className={styles.pill} />
      <Image src={substance} alt="substance" className={styles.substance} />
      <div className={styles.ellipse} />
    </div>
  );
};

export default Feedback;
