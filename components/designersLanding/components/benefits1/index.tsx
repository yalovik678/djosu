import { FC } from "react";
import liquida from "@/public/assets/designers-icons/vacancies/mujer-liquida.png";
import toroid from "@/public/assets/designers-icons/benefits1/toroid-purple.png";
import roundCube from "@/public/assets/designers-icons/benefits1/roundCube.png";
import djosuText3 from "@/public/assets/designers-icons/benefits1/djosu-3.png";
import djosuText3Mobile from "@/public/assets/designers-icons/benefits1/djosu-3-mobile.png";
import blur from "@/public/assets/designers-icons/benefits1/blur.svg";
import groupInfoImage from "@/public/assets/designers-icons/benefits1/group-info-image.png";
import Image from "next/image";

import styles from "./styles.module.scss";
import CustomButton from "@/components/lips/customButton";

const Benefits1: FC = () => {
  return (
    <div className={styles.containerBenefits1}>
      <Image
        src={groupInfoImage}
        alt="groupInfoImage"
        className={styles.groupInfoImage}
      />
      <CustomButton
        text="REGISTRATION"
        className={styles.customButton}
        onClick={() => console.log("here")}
      />
      <div className={styles.text}>
        <div className={styles.tx1}>
          Djosu continues to develop and expand its services to help even more
          people{" "}
        </div>
        <div className={styles.tx2}>find their dream IT jobs.</div>
      </div>
      <div className={styles.become}>Become part of our team now! </div>
      <Image src={liquida} alt="liquida" className={styles.liquida} />
      <Image src={toroid} alt="toroid" className={styles.toroid} />
      <Image src={roundCube} alt="roundCube" className={styles.roundCube} />
      <Image src={djosuText3} alt="djosuText3" className={styles.djosuText3} />
      <Image
        src={djosuText3Mobile}
        alt="djosuText3Mobile"
        className={styles.djosuText3Mobile}
      />
      <Image src={blur} alt="blur" className={styles.blur} />
    </div>
  );
};

export default Benefits1;
