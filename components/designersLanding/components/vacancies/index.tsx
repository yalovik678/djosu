import React, { FC } from "react";
import liquida from "@/public/assets/designers-icons/vacancies/mujer-liquida.png";
import Image from "next/image";

import styles from "./styles.module.scss";
import { useResizeWindow } from "../../../../hooks/useResizeWindow";
import SliderVacanciesMobile from "@/components/designersLanding/components/vacancies/components/sliderVacancies";

const Vacancies: FC = () => {
  const { isMobile } = useResizeWindow();

  return (
    <div className={styles.containerVacancies}>
      <div className={styles.title}>
        <div className={styles.title1}>
          If you are looking for a new challenge ,
        </div>
        <div className={styles.title2}>
          And would like to join a friendly and creative team
        </div>
        <div className={styles.title3}>
          <span>Djosu</span> is your ideal place to find employment.
        </div>
      </div>
      {isMobile && <div className={styles.rectangle} />}
      {isMobile ? (
        <SliderVacanciesMobile vacancies={vacancies} />
      ) : (
        <>
          <div className={styles.info}>
            {vacancies[0].element}
            {vacancies[1].element}
            {vacancies[2].element}
          </div>
        </>
      )}
      <Image src={liquida} alt="liquida" className={styles.liquida} />
    </div>
  );
};

export default Vacancies;

export const vacancies = [
  {
    id: 1,
    element: (
      <div className={`${styles.graphic} ${styles.ux}`}>
        <div className={styles.textMain}>
          <div className={styles.title}>Graphic designer</div>
          <div className={styles.text}>
            Creates visual concepts, designs logos, banners and other graphics.
          </div>
        </div>
        <div className={styles.respond}>Respond</div>
      </div>
    ),
  },
  {
    id: 2,
    element: (
      <div className={styles.ux}>
        <div className={styles.textMain}>
          <div className={styles.title}>UX/UI designer</div>
          <div className={styles.text}>
            Analyzes user needs, designs user-friendly interfaces and
            navigation, and ensures effective interaction with the product.
          </div>
        </div>
        <div className={styles.respond}>Respond</div>
      </div>
    ),
  },
  {
    id: 3,
    element: (
      <div className={`${styles.motion} ${styles.ux}`}>
        <div className={styles.textMain}>
          <div className={styles.title}>Motion designer</div>
          <div className={styles.text}>
            Creates animations and interactive elements for videos,
            presentations and other multimedia projects.
          </div>
        </div>
        <div className={styles.respond}>Respond</div>
      </div>
    ),
  },
  {
    id: 4,
    element: (
      <div className={`${styles.graphic} ${styles.ux}`}>
        <div className={styles.textMain}>
          <div className={styles.title}>Graphic designer</div>
          <div className={styles.text}>
            Creates visual concepts, designs logos, banners and other graphics.
          </div>
        </div>
        <div className={styles.respond}>Respond</div>
      </div>
    ),
  },
  {
    id: 5,
    element: (
      <div className={styles.ux}>
        <div className={styles.textMain}>
          <div className={styles.title}>UX/UI designer</div>
          <div className={styles.text}>
            Analyzes user needs, designs user-friendly interfaces and
            navigation, and ensures effective interaction with the product.
          </div>
        </div>
        <div className={styles.respond}>Respond</div>
      </div>
    ),
  },
  {
    id: 6,
    element: (
      <div className={`${styles.motion} ${styles.ux}`}>
        <div className={styles.textMain}>
          <div className={styles.title}>Motion designer</div>
          <div className={styles.text}>
            Creates animations and interactive elements for videos,
            presentations and other multimedia projects.
          </div>
        </div>
        <div className={styles.respond}>Respond</div>
      </div>
    ),
  },
];
