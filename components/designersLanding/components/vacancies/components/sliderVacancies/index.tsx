import React, { FC, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import styles from "./styles.module.scss";

interface IVacancies {
  id: number;
  element: JSX.Element;
}

interface ISliderMobile {
  vacancies: IVacancies[];
}

const SliderVacanciesMobile: FC<ISliderMobile> = ({ vacancies }) => {
  return (
    <div className={styles.containerMobile}>
      <Swiper
        initialSlide={0}
        slidesPerView={3}
        watchSlidesProgress
        speed={200}
        mousewheel
        keyboard
        loop
        className={styles.mainPhotoSlider}
      >
        {vacancies.map((vacancy) => (
          <SwiperSlide key={vacancy.id} className={styles.imagesWrapper}>
            <label className={styles.slideContainer}>{vacancy.element}</label>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default SliderVacanciesMobile;
